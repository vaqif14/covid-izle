package com.covidizle;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconParser;

import java.lang.reflect.Array;
import java.util.List;

public class BeaconScannerService extends Service {
    static final String PARTIAL_UUID_STRING = "848da4e8-0793-5813";

    public static final String CHANNEL_ID = "BeaconScannerServiceChannel";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    BluetoothLeScanner mBluetoothLeScanner = BluetoothAdapter.getDefaultAdapter().getBluetoothLeScanner();

    BeaconParser beaconParser = new BeaconParser()
            .setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24");

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String uuid = intent.getStringExtra("uuid");
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        Notification notification = mBuilder
                .setContentTitle("COVİD izlə")
                .setContentText("Tətbiq arxa fonda işləyir")
                .setSmallIcon(R.drawable.notification)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);


        mBluetoothLeScanner.startScan(mScanCallback);


        return START_NOT_STICKY;
    }


    ReactContext getReactApplicationContext() {
        MainApplication application = (MainApplication) this.getApplication();

        ReactNativeHost reactNativeHost = application.getReactNativeHost();
        ReactInstanceManager reactInstanceManager = reactNativeHost.getReactInstanceManager();
        ReactContext reactContext = reactInstanceManager.getCurrentReactContext();

        return reactContext;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
//        stopScanning();
//        stopSelf();
//
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onDestroy() {
//        stopScanning();
//
        super.onDestroy();
    }

    void stopScanning() {
        if (this.mBluetoothLeScanner != null) {
            this.mBluetoothLeScanner.stopScan(mScanCallback);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    ScanCallback mScanCallback = new ScanCallback() {

        WritableMap getReadableMap(Beacon beacon) {

            WritableMap params = Arguments.createMap();


            String deviceId = beacon.getIdentifier(0).toString();
            double distance = beacon.getDistance();
//            Log.i("BLE", String.valueOf(distance) + " " + deviceId);
            params.putString("deviceId", deviceId);
            params.putInt("rssi", beacon.getRssi());

            params.putDouble("distance", distance);

            int txPower = beacon.getTxPower();
            params.putInt("txPower", txPower);


            return params;

        }


        WritableMap getReadableMap(ScanResult result) {

            WritableMap params = Arguments.createMap();
            List serviceUUIDs = result.getScanRecord().getServiceUuids();

            if (serviceUUIDs != null) {
                String deviceId = serviceUUIDs.get(0).toString();
                double distance = 0;
//                Log.i("BLE", String.valueOf(distance) + " " + deviceId);
                params.putString("deviceId", deviceId);
                params.putInt("rssi", result.getRssi());

                params.putDouble("distance", distance);

                int txPower = 0;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    txPower = result.getTxPower();
                }
                params.putInt("txPower", txPower);

            } else {
//                Log.i("BLE", "beacon nil");

            }

            return params;

        }


        WritableMap handleScanResultParsing(ScanResult result) {
            if (result == null || result.getDevice() == null) {
                return null;
            }

            Beacon beacon = beaconParser.fromScanData(result.getScanRecord().getBytes(), result.getRssi(), result.getDevice(), result.getTimestampNanos());
            List serviceUUIDs = result.getScanRecord().getServiceUuids();
//           Log.d("Iphone", String.valueOf(result.getScanRecord().getServiceUuids()));

            // Signal Could be from android
            if (beacon != null && beacon.getIdentifier(0) != null) {
                String deviceId = beacon.getIdentifier(0).toString();
                // Definitely from android
                if (deviceId != null) {
//                    Log.i("BLE", "Traced a signal from android");
                    return this.getReadableMap(beacon);

                }
            }
            // Signal Could be from iOS
            else if (serviceUUIDs != null) {
                Object deviceUUID = result.getScanRecord().getServiceUuids();
                // Definitely from IOS
                if (deviceUUID != null) {
//                    Log.i("BLE", "Traced a signal from iOS");
                    return this.getReadableMap(result);
                }
            }

            return null;

        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        public void onScanResult(int callbackType, ScanResult result) {

            WritableMap params = this.handleScanResultParsing(result);

            if (params != null) {
                ReactContext currentContext = getReactApplicationContext();
                currentContext
                        .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                        .emit("onScanResult", params);

            }


        }


        @Override
        public void onBatchScanResults(List<ScanResult> results) {
//            Log.i("BLE", "Batch results " + results);
            WritableArray params = Arguments.createArray();
            for (ScanResult result : results) {
                WritableMap map = this.handleScanResultParsing(result);
                if (params != null) {
                    params.pushMap(map);
                }
            }

            if (params.size() > 0) {
                ReactContext currentContext = getReactApplicationContext();

                currentContext
                        .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                        .emit("onScanResultBulk", params);
            }


        }

        @Override
        public void onScanFailed(int errorCode) {
            WritableMap params = Arguments.createMap();
            params.putInt("error", errorCode);
            ReactContext currentContext = getReactApplicationContext();

            currentContext
                    .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                    .emit("onScanFailed", params);

//            Log.e("BLE", "Discovery onScanFailed: " + errorCode);
        }
    };


    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Scanner Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            serviceChannel.setSound(null, null);
            serviceChannel.setShowBadge(false);
            manager.createNotificationChannel(serviceChannel);
        }
    }
}