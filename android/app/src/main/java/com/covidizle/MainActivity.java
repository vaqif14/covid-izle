package com.covidizle;

import com.reactnativenavigation.NavigationActivity;
import android.content.Intent;
import android.util.Log;

public class MainActivity extends NavigationActivity {

    @Override
    protected void onDestroy() {
        Log.i("Destroy", "Main app destroy");
        Intent serviceIntent = new Intent(getApplicationContext(), BeaconTransmitterService.class);
        Intent scanServiceIntent = new Intent(getApplicationContext(), BeaconScannerService.class);
        Intent module = new Intent(getApplicationContext(), BeaconModule.class);
        Intent pack = new Intent(getApplicationContext(), BeaconPackage.class);

        getApplicationContext().startService(serviceIntent);
//        getApplicationContext().startService(module);
//        getApplicationContext().startService(pack);
//        getApplicationContext().startService(scanServiceIntent);

        super.onDestroy();
    }
}
