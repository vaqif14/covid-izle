import { StyleSheet, Dimensions } from 'react-native'

export const NAV_TAB_HEIGHT = 95
export const NAV_HEIGHT = 45
export const TAB_HEIGHT = 50

export const deviceHeight = Dimensions.get('window').height
export const deviceWidth = Dimensions.get('window').width
export const shadowOpt = { btnWidth: deviceWidth - 55, btnHeight: 45 }

export const spaceHeight = deviceHeight - 375 - 45

export const introSpaceHeight = deviceHeight - 364

// CommonStyles
export default CommonStyles = StyleSheet.create({
  normalPage: {
    position: 'relative',
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  normalSinglePage: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  wrapperBox: {
    marginTop: 20,
    marginBottom: 20,
  },
  noTabScrollView: {
    marginTop: NAV_HEIGHT,
  },
  smallWrapperBox: {
    marginTop: 20,
    marginBottom: 20,
  },
  scrollView: {
    marginTop: NAV_HEIGHT,
    marginBottom: TAB_HEIGHT,
  },
  // Color
  whiteColor: {
    color: '#FFFFFF',
  },
  greyColor: {
    color: '#9D9F9F',
  },
  lightgreyColor: {
    color: 'rgb(150,150,150)',
  },
  darkGrey: {
    color: '#383838',
  },
  blackColor: {
    color: 'rgb(19,19,19)',
  },
  softBlueColor: {
    color: 'rgb(75,102,234)',
  },
  darkSkyBlueColor: {
    color: 'rgb(63,103,230)',
  },
  periBlueColor: {
    color: 'rgb(79,109,230)',
  },
  // Text
  extraLargeText: {
    height: 52,
    fontSize: 32,
    fontFamily: 'Roboto-Bold',
  },
  titleText: {
    fontSize: 30,
    lineHeight: 38,
  },
  headerText: {
    fontSize: 16,
    lineHeight: 19,
  },
  itemHeaderText: {
    fontSize: 17,
    lineHeight: 29,
  },
  mediumText: {
    fontSize: 16,
  },
  normalText: {
    fontSize: 15,
    lineHeight: 23,
  },
  smallText: {
    fontSize: 13,
    lineHeight: 30,
  },
  shortSmallText: {
    fontSize: 13,
    lineHeight: 23,
  },
  light: {
    fontFamily: 'Roboto-Light',
  },
  regularBold: {
    fontFamily: 'Roboto-Regular',
  },
  mediumBold: {
    fontFamily: 'Roboto-Medium',
  },
  semiBold: {
    fontFamily: 'Roboto-SemiBold',
  },
  extraBold: {
    fontFamily: 'Roboto-Bold',
  },
  // Image
  borderRadius: {
    borderRadius: 200,
  },
  // Item box
  itemWhiteBox: {
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 8,
    backgroundColor: '#FFFFFF',
    elevation: 6,
  },
  // Form Styles
  textInputField: {
    flexDirection: 'row',
    width: deviceWidth - 55,
    height: 45,
    marginBottom: 25,
    borderColor: 'rgb(229,229,229)',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 50,
    backgroundColor: '#FFFFFF',
  },
  textInput: {
    width: deviceWidth - 55,
    height: 45,
    paddingLeft: 50,
    color: 'rgb(150,150,150)',
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
  },
  selectboxField: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: deviceWidth - 55,
    height: 45,
    paddingLeft: 20,
    paddingRight: 20,
    borderColor: 'rgb(229,229,229)',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 50,
    backgroundColor: '#FFFFFF',
  },
  selectboxLabel: {
    color: 'rgb(150,150,150)',
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
  },
  // Button Styles
  backButton: {
    flex: 1,
    alignItems: 'center',
    width: 41,
    height: 41,
  },
  nextButton: {
    flex: 1,
    alignItems: 'center',
    width: 60,
    height: 60,
  },
  buttonStyle: {
    width: '100%',
    borderRadius: 4,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    height: 36,
  },
  // margin
  mt1: {
    marginTop: 10,
  },
  mt2: {
    marginTop: 20,
  },
  mt3: {
    marginTop: 30,
  },
  mt4: {
    marginTop: 40,
  },
  mt5: {
    marginTop: 50,
  },
  mb1: {
    marginBottom: 10,
  },
  mb2: {
    marginBottom: 20,
  },
  mb3: {
    marginBottom: 30,
  },
  mb4: {
    marginBottom: 40,
  },
  mb5: {
    marginBottom: 50,
  },

  // StartScreens
  labelField: {
    marginTop: 30 + NAV_HEIGHT,
    marginBottom: 25,
    paddingLeft: (deviceWidth - (deviceWidth - 55)) / 2,
  },
})
