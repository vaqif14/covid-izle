import { Dimensions } from 'react-native'

export const NAV_TAB_HEIGHT = 95
export const NAV_HEIGHT = 45
export const TAB_HEIGHT = 50

export const deviceHeight = Dimensions.get('window').height
export const deviceWidth = Dimensions.get('window').width
export const shadowOpt = { btnWidth: deviceWidth - 55, btnHeight: 45 }

export const spaceHeight = deviceHeight - 375 - 45

export const introSpaceHeight = deviceHeight - 364

export const Colors = {
  primary: '#1AC984',
  success: '#7BBA16',
  info: '#1168FF',
  warning: '#ffd800',
  danger: '#ff3269',
}
