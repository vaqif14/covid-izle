import styled from 'styled-components/native'
import { Icon as BaseIcon, Input as BaseInput } from 'native-base'
import * as system from 'styled-system'

export const ScrollView = styled.ScrollView`
  ${system.layout}
  ${system.color}
`

export const View = styled.View`
	${system.position}
	${system.layout}
	${system.flexbox}
	${system.grid}
	${system.color}
	${system.border}
	${system.shadow}
	${system.space}
`

export const Text = styled.Text`
	${system.layout}
	${system.position}
	${system.flexbox}
	${system.border}
	${system.color}
	${system.typography}
	${system.space}
`

export const Button = styled.TouchableOpacity`
	${system.position}
	${system.layout}
	${system.flexbox}
	${system.color}
	${system.border}
	${system.shadow}
	${system.space}
`

export const Input = styled(BaseInput)`
	${system.position}
	${system.layout}
	${system.flexbox}
	${system.color}
	${system.border}
	${system.shadow}
	${system.typography}
	${system.space}
`

export const Image = styled.Image`
	${system.position}
	${system.layout}
	${system.color}
	${system.border}
	${system.shadow}
	${system.space}
	${system.background}
`

export const Icon = styled(BaseIcon)`
	${system.position}
	${system.layout}
	${system.color}
	${system.typography}
	${system.border}
	${system.shadow}
	${system.space}
`
