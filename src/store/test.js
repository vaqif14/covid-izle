import { createReducer, createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  navLayout: ['payload'],
})

export default Creators

const INITIAL_STATE = { nav_status: null, nav_color: null }

const navLayout = (state, action) => {
  return {
    ...state,
    nav_status: action.payload.status,
    nav_color: action.payload.color,
  }
}

const HANDLERS = {
  [Types.NAV_LAYOUT]: navLayout,
}

export const reducer = createReducer(INITIAL_STATE, HANDLERS)
