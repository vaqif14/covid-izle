// @flow

import { persistCombineReducers } from 'redux-persist'
import { resettableReducer } from 'reduxsauce'
import AsyncStorage from '@react-native-community/async-storage'

const persistConfig = {
  active: true,
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['btStatus', 'getSms', 'shareContacs', 'netStatus'],
  whitelist: ['regUser', 'localUser'],
}

const resetContact = resettableReducer('RESET_CONTACT')
const resetOffline = resettableReducer('RESET_OFFLINE_DATA')
const reqUserReset = resettableReducer('RESET_REG_USER')

const appReducer = persistCombineReducers(persistConfig, {
  btStatus: require('../components/ble/redux').reducer,
  getSms: require('../redux/Registration/redux').reducer,
  netStatus: require('../redux/Dashboard/redux').reducer,
  sickList: resetOffline(
    require('../redux/userSevice/sickListReducer').reducer
  ),
  shareContacs: resetContact(require('../redux/Contact/redux').reducer),
  regUser: reqUserReset(require('../redux/Registration/redux').reducer),
})

export default function rootReducer(state, action) {
  return appReducer(state, action)
}
