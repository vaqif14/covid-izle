//  @flow

import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { compact } from 'lodash'
import { persistStore } from 'redux-persist'
import createSagaMiddleware from 'redux-saga'

import rootReducer from './reducers'
import sagas from './sagas'

export default function initializeStore() {
  const sagaMiddleware = createSagaMiddleware()

  const middlewares = compact([thunk.withExtraArgument(), sagaMiddleware])



  const store = createStore(
    rootReducer,
    {},
    compose(applyMiddleware(...middlewares))
  )

  persistStore(store, null, () => {
    store.getState()
  })

  sagaMiddleware.run(sagas)

  return store
}
