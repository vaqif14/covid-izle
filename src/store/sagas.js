import { all } from 'redux-saga/effects'
import authSaga from '../redux/Registration/saga'
import shareContacts from '../redux/Contact/saga'
export default function* rootSaga() {
  yield all([authSaga(), shareContacts()])
}
