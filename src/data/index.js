import SQLite from 'react-native-sqlite-storage'

export class BaseManager {
    constructor() {
        this.sqlite = SQLite;
        this.sqlite.DEBUG(true)
        this.sqlite.openDatabase({
            name: 'Covid19',
            location: 'db.sqlite'
        }).then(db => {
            this.dbInstance = db
        })
    }

    createTable() {
        return new Promise((resolve, reject) => {
            this.dbInstance.executeSql(
                "CREATE TABLE covid19 (" +
                "head_chunk_number INTEGER," +
                "tail_chunk_number	INTEGER," +
                "sick_list	TEXT);"
            ).then(val => {
                resolve(true)
            }).catch(err => {
                console.log(err)
            })
        })
    }
}