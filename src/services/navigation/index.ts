import { Layout, Navigation } from 'react-native-navigation'

export const setRoot = (type: string, id: string, children: Layout[]) =>
  Navigation.setRoot({
    root: {
      [type]: {
        id,
        children,
      },
    },
  })
