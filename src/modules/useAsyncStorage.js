import { useState, useEffect } from 'react'
import AsyncStorage from '@react-native-community/async-storage'

function useAsyncStorage(key, initialValue) {
  const [storedValue, setStoredValue] = useState(
    initialValue instanceof Function ? initialValue() : initialValue
  )
  // Sham: why it's necessary to call useEffect instead direct call ?
  useEffect(() => {
    AsyncStorage.getItem(key)
      .then((value) => {
        if (value !== null)
          try {
            value = JSON.parse(value)
            return initialValue instanceof Function
              ? initialValue(value)
              : value !== null
              ? value
              : initalValue
          } catch {}
        return storedValue
      })
      .then(setStoredValue)
  }, [key, initialValue])

  const setValue = (value) => {
    const valueToStore = value instanceof Function ? value(storedValue) : value
    setStoredValue(valueToStore)
    AsyncStorage.setItem(key, JSON.stringify(valueToStore))
  }

  return [storedValue, setValue]
}

export default useAsyncStorage
