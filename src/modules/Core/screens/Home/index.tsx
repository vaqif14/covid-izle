import React, { FunctionComponent } from 'react'
import { Navigation } from 'react-native-navigation'
import { Toast } from 'native-base'
import { USER_KEY } from 'config'
import { Button, Container } from 'components'
import { H1, Paragraph } from 'components/Text'
import { goToAuthScreen } from 'navigation'
import { IProps } from './types'

export const HOME_SCREEN = {
  name: 'app.Home',
  title: 'ANA SEHIFE',
}

export const HomeScreen: FunctionComponent<IProps> = ({ componentId }) => {
  const handleLogOut = async () => {
    try {
      await goToAuthScreen()
    } catch (error) {
      Toast.show({
        text: 'Logout failed!',
        type: 'danger',
        position: 'top',
      })
    }
  }

  return (
    <Container marginHorizontal={20} marginVertical={20}>
      <H1>Home Screen</H1>
    </Container>
  )
}

// @ts-ignore
HomeScreen.options = () => ({
  topBar: {
    title: {
      text: HOME_SCREEN.title,
    },
  },
})
