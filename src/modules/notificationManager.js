import React, { useEffect } from "react";
import { Notifications } from "react-native-notifications";
import AsyncStorage from "@react-native-community/async-storage";
import { ApiCall } from "../redux/constant";

function Notify() {
  useEffect(() => {
    Notifications.registerRemoteNotifications();
    Notifications.events().registerRemoteNotificationsRegistered(
      async (event) => {
        let token = await AsyncStorage.getItem("token");
        let a = await ApiCall.post(
          "/common/push",
          { pushToken: event.deviceToken },
          {
            headers: {
              Accept: "application/json",
              "X-Token": token,
            },
          }
        );
        console.log({ token, a });
      }
    );
    Notifications.events().registerRemoteNotificationsRegistrationFailed(
      (event) => {
        console.error(event);
      }
    );
  }, []);
  return null;
}

export default Notify;
