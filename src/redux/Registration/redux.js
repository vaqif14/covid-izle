import { createReducer, createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  reg_user: ['payload'],
  reg_data: ['payload'],
  get_sms: ['sms'],
  getCryptUUID: ['cryptUUID'],
  getToken: ['token'],
  getUUID: ['uuid'],
  getError: ['error'],
  getLoading: ['loading'],
})

export default Creators

const INITIAL_STATE = {
  id: '',
  number: '',
  age: '',
  gender: '',
  smsCode: '',
  token: '',
  uuid: '',
  cryptUUID: '',
  error: false,
  loading: false,
}

const regUser = (state, action) => {
  return {
    ...state,
    id: action.payload.id,
    number: action.payload.number,
    age: action.payload.age,
    gender: action.payload.gender,
    loading: action.payload.loading,
  }
}
const regData = (state, action) => {
  return {
    ...state,
    id: action.payload.id,
    number: action.payload.number,
    age: action.payload.age,
    gender: action.payload.gender,
  }
}
const getSms = (state, { sms }) => {
  return {
    ...state,
    smsCode: sms,
  }
}

export const token = (state, { token }) => {
  return {
    ...state,
    token: token,
  }
}
export const loading = (state, { loading }) => {
  return {
    ...state,
    loading: loading,
  }
}
export const UUID = (state, { uuid }) => {
  return {
    ...state,
    uuid: uuid,
  }
}
export const cryptUUID = (state, { cryptUUID }) => {
  return {
    ...state,
    cryptUUID: cryptUUID,
  }
}
export const error = (state, { error }) => {
  return {
    ...state,
    error: error,
  }
}

const HANDLERS = {
  [Types.REG_USER]: regUser,
  [Types.REG_DATA]: regData,
  [Types.GET_SMS]: getSms,
  [Types.GET_TOKEN]: token,
  [Types.GET_UUID]: UUID,
  [Types.GET_CRYPT_UUID]: cryptUUID,
  [Types.GET_ERROR]: error,
  [Types.GET_LOADING]: loading,
}

export const reducer = createReducer(INITIAL_STATE, HANDLERS)
