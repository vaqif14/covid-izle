import {all, call, fork, put, select, takeEvery} from 'redux-saga/effects';
import AsyncStorage from '@react-native-community/async-storage';
import {encryptData} from '../userSevice/crypt';
import {Navigation} from 'react-native-navigation';
import {ApiCall} from '../constant';
import {goToHomeScreen} from '../../navigation';
import RegActions from './redux';

function* watchRegistration() {
  yield takeEvery('REG_USER', makePhoneRegister);
}

const makeAsyncRegister = async (number, age, gender) =>
  await ApiCall.post(
    '/user/register',
    {
      gender: gender,
      age: parseInt(age),
      phone: number,
    },
    {
      headers: {
        Accept: 'application/json',
      },
    },
  );

function* makePhoneRegister({payload}) {
  const {number, age, gender, id} = payload;
  try {
    const login = yield call(makeAsyncRegister, number, age, gender);

    if (login.status == 200) {
      const {user, token} = login.data;
      yield put(RegActions.getToken(token));
      yield put(RegActions.getUUID(user));
      yield put({type: 'GET_LOADING', loading: false});
      Navigation.push(id, {
        component: {
          name: 'PinScreen',
          options: {
            topBar: {
              visible: false,
            },
          },
        },
      });
    } else {
      yield put({type: 'GET_LOADING', loading: false});
      yield put({type: 'GET_ERROR', error: true});
    }
  } catch (err) {
    yield put({type: 'GET_LOADING', loading: false});
    yield put({type: 'GET_ERROR', error: true});
  }
}

function* watchSendAgain() {
  yield takeEvery('SEND_AGAIN', sendAgain);
}

function* sendAgain() {
  try {
    const {
      regUser: {number, age, gender},
    } = yield select();
    const login = yield call(makeAsyncRegister, number, age, gender);
    if (login.status == 200) {
      const {user, token} = login.data;
      yield put(RegActions.getToken(token));
      yield put(RegActions.getUUID(user));
    }
  } catch (e) {}
}
function* watchSms() {
  yield takeEvery('GET_SMS', smsRegistration);
}

const getSmsAsync = (sms, token) =>
  ApiCall.post(
    '/user/verify',
    {code: sms},
    {
      headers: {
        Accept: 'application/json',
        'X-Token': token,
      },
    },
  );

const goDashboardAsync = async () => await goToHomeScreen();
const setTokenAsync = async token => await AsyncStorage.setItem('token', token);
const setUuidAsync = async uuid => await AsyncStorage.setItem('UUID', uuid);

function* smsRegistration({sms}) {
  try {
    const {
      regUser: {token, uuid},
    } = yield select();
    yield put(RegActions.getError(false));
    const getSms = yield call(getSmsAsync, sms, token);
    if (getSms.status === 204) {
      yield put(RegActions.getError(false));
      yield call(setTokenAsync, token);
      yield call(setUuidAsync, uuid);
      yield call(goDashboardAsync);
    }

    if (getSms.status === 400) {
      yield put({type: 'GET_ERROR', error: true});
    }
  } catch (err) {
    yield put({type: 'GET_ERROR', error: true});
  }
}

export default function* rootSaga() {
  yield all([fork(watchRegistration), fork(watchSms), fork(watchSendAgain)]);
}
