import Api from 'apisauce'

const baseURL = 'https://covid.economy.gov.az/api/1'

export const ApiCall = Api.create({
  baseURL: baseURL,
})
