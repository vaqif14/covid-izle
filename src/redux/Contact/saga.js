import {
  all,
  call,
  fork,
  delay,
  put,
  select,
  takeEvery,
} from 'redux-saga/effects';
import {ApiCall} from '../constant';

function* watchContactReceived() {
  yield takeEvery('SHARED_USERS_CONFIRM', shareApp);
}

const inviteAsync = async (users, token) =>
  ApiCall.post('/common/invite', users, {
    headers: {
      Accept: 'application/json',
      'X-Token': token,
    },
  });
function* shareApp({}) {
  try {
    const {
      regUser: {token},
      shareContacs: {users},
    } = yield select();
    const invite = yield call(inviteAsync, users, token);
    console.log(invite);

    if (invite.status == 204) {
      yield put({type: 'SUCCESS_CONTACT', success: true, error: false});
      yield delay(3000);
      yield put({type: 'RESET_CONTACT'});
    }
    if (invite.status == 400) {
      yield put({type: 'SUCCESS_CONTACT', success: false, error: true});
    }
  } catch (e) {}
}

export default function* rootSaga() {
  yield all([fork(watchContactReceived)]);
}
