import { createReducer, createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  shared_users: ['users'],
  successContact: ['success'],
})

export default Creators

const INITIAL_STATE = {
  users: [],
  success: false,
  error: false,
}

export const sharedUsers = (state, { users }) => {
  return {
    ...state,
    users: users,
  }
}
export const success = (state, { success, error }) => {
  return {
    ...state,
    success: success,
    error: error,
  }
}
const HANDLERS = {
  [Types.SHARED_USERS]: sharedUsers,
  [Types.SUCCESS_CONTACT]: success,
}

export const reducer = createReducer(INITIAL_STATE, HANDLERS)
