import { createReducer, createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  UUIDdevice: ['uuid'],
})

export default Creators

const INITIAL_STATE = {
  stored_users: [],
}

const devicesUUID = (state, { uuid }) => {
  return {
    ...state,
    uuid: uuid,
  }
}

export const localData = createReducer(INITIAL_STATE, {
  [Types.STORED_USERS]: storedUsers,
})
