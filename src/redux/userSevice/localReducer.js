import { createReducer, createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  storedUsers: ['stored'],
})

export default Creators

const INITIAL_STATE = {
  stored_users: [],
}

const storedUsers = (state, { stored }) => {
  return {
    ...state,
    stored_users: stored == null ? [] : state.stored_users.concat(stored),
  }
}

export const localData = createReducer(INITIAL_STATE, {
  [Types.STORED_USERS]: storedUsers,
})
