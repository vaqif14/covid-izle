import { createReducer, createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  sendUser: ['send'],
  successUser: ['success'],
  errorUser: ['error'],
})

export default Creators

const INITIAL_STATE = {
  stored_users: [],
  send_user: [],
  success: true,
  error: false,
}

const sendUser = (state, { send }) => {
  return {
    send_user: send,
  }
}
const succes = (state, { success }) => {
  return {
    ...state,
    success: success,
  }
}
const errorUser = (state, { error }) => {
  return {
    ...state,
    error: error,
  }
}

const HANDLERS = {
  [Types.SUCCESS_USER]: succes,
  [Types.ERROR_USER]: errorUser,
  [Types.SEND_USER]: sendUser,
}

export const reducer = createReducer(INITIAL_STATE, HANDLERS)
