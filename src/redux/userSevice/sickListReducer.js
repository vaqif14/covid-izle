import { createReducer, createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  sickList: ['sick'],
  onlineSickList: ['online_sick_list'],
  offlineSickList: ['offline_sick_list'],
  showMessage: ['message'],
  offlineSuccess: ['success'],
  showSickButton: ['message'],
  offlineError: ['error'],
})

export default Creators

const INITIAL_STATE = {
  sickList: [],
  localSickList: [],
  messageType: '',
  messageContent: '',
  messageHeader: '',
  showMessage: false,
  messageId: '',
  buttonPrimary: [],
  buttonSecondary: [],
  sickButton: false,
}

const onlineSickList = (state, { sickUsers }) => {
  return {
    sickList: sickUsers,
  }
}
const showMessage = (state, { message }) => {
  return {
    ...state,
    messageType: message.messageType,
    messageContent: message.messageContent,
    messageHeader: message.messageHeader,
    showMessage: message.showMessage,
    messageId: message.messageId,
    buttonPrimary: message.buttonPrimary,
    buttonSecondary: message.buttonSecondary,
  }
}
const offlineSickList = (state, { success }) => {
  return {
    ...state,
    success: success,
  }
}
const showSickBtn = (state, { error }) => {
  return {
    ...state,
    error: error,
  }
}

const HANDLERS = {
  [Types.ONLINE_SICK_LIST]: onlineSickList,
  [Types.SHOW_MESSAGE]: showMessage,
  [Types.SHOW_SICK_BUTTON]: showSickBtn,
}

export const reducer = createReducer(INITIAL_STATE, HANDLERS)
