import {
  all,
  delay,
  call,
  fork,
  put,
  select,
  takeEvery,
} from 'redux-saga/effects'
import userAction from './redux'
import axios from 'axios'
import { ApiCall } from '../constant'
import { encryptData } from './crypt'
import { id } from 'date-fns/locale'



function* watchUserData() {
  yield takeEvery('SEND_USER', getUsers)
}



const userStatusAsync = async (data, token) =>
  await axios.post('https://covid.economy.gov.az/api/1/user/status', data, {
    headers: {
      'Content-Type': 'text/plain; charset=utf-8',
      'X-Token': token,
    },
  })

function* getUsers({ send }) {
  const {
    regUser: { token, uuid },
    netStatus: { net_status },
    localUser: { stored_users },
    sickList: { sickList },
  } = yield select()
  const a = send
    .filter((f, i) => send.findIndex((fi) => f.data === fi.data) === i)
    .map((item) => {
      const scans = send.filter((f) => f.data === item.data).length
      item.data = {
        [`${item.data}/${uuid}`]: scans,
      }
      item.scans = scans
      return item
    })
  try {
    if (net_status) {
      let res = yield call(
        userStatusAsync,
        encryptData(stored_users.length > 1 ? stored_users : a),
        token
      )
      if (res.status == 200) {
        yield put({ type: 'ONLINE_SICK_LIST', sickUsers: res.data.sickList })
        if (res.data.showMessage) {
          yield put({ type: 'RESET_STORED_USER' })
          const {
            showMessage: { id, type, content, header, buttons },
          } = res.data
          yield put({
            type: 'SHOW_MESSAGE',
            message: {
              messageId: id,
              messageType: type,
              messageContent: content,
              messageHeader: header,
              buttonPrimary: buttons[0],
              buttonSecondary: buttons[1],
              showMessage: true,
            },
          })
        }
        if (res.data.showSickButton) {
          yield put({ type: 'SHOW_SICK_BUTTON', sickButton: true })
        }
      }
    } else {
      if (sickList.length > 1) {
        let checkSickArr = []
        send
          .filter((f, i) => send.findIndex((fi) => f.data === fi.data) === i)
          .map((item) => {
            if (sickList.includes(item.data)) {
              const count = send.filter((f) => f.data === item.data).length
              item.data = { [`${item.data}/${uuid}`]: count }
              item.scans = count
              checkSickArr.push(item)
            }
          })
        yield put({ type: 'STORED_USERS', stored: checkSickArr })
        yield delay(60000)
        yield put({ type: 'SUCCESS_USER', success: true })
      } else {
        yield put({ type: 'STORED_USERS', stored: a })
        yield delay(60000)
        yield put({ type: 'SUCCESS_USER', success: true })
      }
    }
    yield delay(60000)
    yield put({ type: 'SUCCESS_USER', success: true })
  } catch (e) {
    console.log(e)
    yield put({ type: 'STORED_USERS', stored: a })
    yield delay(60000)
    yield put({ type: 'SUCCESS_USER', success: true })
  }
}

function* watchSendMessage() {
  yield takeEvery('SEND_MESSAGE', sendMessage)
}

const showMessageAsync = async (id, data, token) =>
  await axios.post(
    'https://covid.economy.gov.az/api/1/message/status',
    { id: id, data: data },
    {
      headers: {
        'Content-Type': 'application/json',
        'X-Token': token,
      },
    }
  )

function* sendMessage({ data }) {
  try {
    const {
      regUser: { token },
      sickList: { messageId },
    } = yield select()
    yield call(showMessageAsync, messageId, data, token)
    yield put({
      type: 'SHOW_MESSAGE',
      message: {
        showMessage: false,
      },
    })
  } catch (e) {
    console.log(e)
  }
}
export default function* rootSaga() {
  yield all([fork(watchUserData), fork(watchSendMessage)])
}
