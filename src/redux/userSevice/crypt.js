import CryptoJS from 'crypto-js'

export function encryptData(state) {
  const cryptedText = CryptoJS.AES.encrypt(
    JSON.stringify(state),
    `!QuX5dBj8TAInpW8EpdlAmz7FFNrEfrKWAayfHdl0EFrp$Ixe&`
  )
  return cryptedText.toString()
}
export function decryptData(state) {
  if (typeof state !== 'string') return null
  try {
    const bytes = CryptoJS.AES.decrypt(
      state,
      `!QuX5dBj8TAInpW8EpdlAmz7FFNrEfrKWAayfHdl0EFrp$Ixe&`
    )
    const decrypted = bytes.toString()
    return JSON.parse(decrypted)
  } catch {}
  return null
}
