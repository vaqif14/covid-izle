import { createReducer, createActions } from 'reduxsauce'

const { Types, Creators } = createActions({
  net_status: ['status'],
})

export default Creators

const INITIAL_STATE = {
  net_status: false,
}

const netStat = (state, { status }) => {
  return {
    ...state,
    net_status: status,
  }
}

const HANDLERS = {
  [Types.NET_STATUS]: netStat,
}

export const reducer = createReducer(INITIAL_STATE, HANDLERS)
