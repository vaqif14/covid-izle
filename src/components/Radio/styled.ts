import styled from 'styled-components/native'
import { Radio } from 'native-base'

export const StyledRadio = styled(Radio)`
  margin-right: 15px;
  position: absolute;
  right: 0;
`
