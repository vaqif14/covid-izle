import React, { FunctionComponent } from 'react'
import {
  NativeBase,
  Container,
  View,
  Text,
  Right,
  Left,
  Content,
} from 'native-base'
import { StyledRadio } from './styled'

export const Radio: FunctionComponent<NativeBase.Radio> = ({
  selected,
  ...props
}) => (
  <StyledRadio
    color="#5EC68B"
    selectedColor="#5EC68B"
    selected={selected}
    {...props}
  />
)
