import React from 'react'
import { View, ScrollView, StyleSheet, Image, Text } from 'react-native'
import { Button } from 'native-base'
import Clock from '../../assets/img/clockSm.png'
import CommonStyles from '../../styles/CommonStyles'
import Phone from '../../assets/img/phone.png'
import PhoneWhite from '../../assets/img/phoneWhite.png'
const ExpandedCard = ({
  imgSource,
  bg,
  description,
  title,
  subText,
  icon,
  time,
  primaryIcon,
  primaryBtnText,
  primaryBtnCallback = null,
  secondaryIcon,
  secondaryBtnText = null,
  secondaryBtnCallback = null,
}) => {
  return (
    <View style={CommonStyles.mb2}>
      <View style={[styles.conatiner, { backgroundColor: bg }]}>
        <Image style={{ position: 'absolute' }} source={imgSource} />
        <View style={{ flexDirection: 'row' }}>
          <Image source={icon} />
          <Text style={styles.headingText}>{title}</Text>
        </View>
        <Text style={styles.subText}>{subText}</Text>
      </View>
      <View style={[styles.subContainer, { borderColor: bg }]}>
        {description ? (
          <>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
              }}
            >
              <Text style={[styles.iconText]}>Bugün, </Text>
              <Image source={Clock} />
              <Text style={styles.iconText}>{time}</Text>
            </View>
            <ScrollView>
              <Text
                style={[
                  CommonStyles.mt2,
                  styles.subText,
                  CommonStyles.darkGrey,
                ]}
              >
                {description}
              </Text>
            </ScrollView>
          </>
        ) : (
          <></>
        )}

        <View style={styles.btnContainer}>
          {primaryBtnText && (
            <Button
              onPress={primaryBtnCallback}
              style={[
                CommonStyles.buttonStyle,
                {
                  backgroundColor: bg,
                  flexDirection: 'row',
                  justifyContent: 'center',
                },
              ]}
            >
              {primaryIcon && (
                <Image
                  source={PhoneWhite}
                  height={10}
                  width={10}
                  style={{ marginRight: 10 }}
                />
              )}
              <Text style={CommonStyles.whiteColor}>{primaryBtnText}</Text>
            </Button>
          )}
          {secondaryBtnText && (
            <Button
              onPress={secondaryBtnCallback}
              style={[CommonStyles.buttonStyle, { backgroundColor: '#DCE5E5' }]}
            >
              {secondaryIcon && (
                <Image
                  source={Phone}
                  height={10}
                  width={10}
                  style={{ marginRight: 10 }}
                />
              )}
              <Text style={[CommonStyles.darkGrey]}>{secondaryBtnText}</Text>
            </Button>
          )}
        </View>
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  conatiner: {
    backgroundColor: 'rgba(219, 139, 0, 0.8)',
    position: 'relative',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    // height: 140,
    padding: 16,
    shadowColor: 'rgba(219, 139, 0, 0.3)',
  },
  headingText: {
    color: '#fff',
    fontSize: 24,
    fontWeight: '700',
    lineHeight: 32,
    marginLeft: 10,
  },
  subText: {
    color: '#fff',
    fontSize: 15,
    fontWeight: '400',
    lineHeight: 20,
    marginTop: 10,
  },
  subContainer: {
    padding: 16,
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignItems: 'flex-start',
    position: 'relative',
    // height: 300,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    borderWidth: 1,
    marginBottom: 20,
  },
  iconText: {
    textAlign: 'left',
    fontSize: 13,
    color: '#9D9F9F',
    marginLeft: 5,
  },
  btnContainer: {
    width: '100%',
  },
})
export default ExpandedCard
