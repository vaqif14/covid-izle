function getDistance(rssi, txPower = 59) {
  if (rssi === 0) {
    return -1.0
  }

  const ratio = (Math.abs(rssi) * 1.0) / Math.abs(txPower)
  if (ratio < 1.0) {
    return Math.pow(ratio, 10)
  } else {
    const accuracy = 0.89976 * Math.pow(ratio, 7.7095) + 0.111
    return accuracy
  }
}

export { getDistance }
