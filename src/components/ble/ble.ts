import {
  NativeModules,
  DeviceEventEmitter,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const UUID = 'c5263db3-131a-45b7-8dfb-d6add0071be0';

type Events =
  | 'onScanResult'
  | 'onScanResultBulk'
  | 'onScanFailed'
  | 'onBroadcastSuccess'
  | 'onBroadcastFailure'
  | 'onStopScanning'
  | 'onStopBroadcast';

const emitter = DeviceEventEmitter;

interface IBLE {
  startBroadcast: () => void;
  stopBroadcast: () => void;
  startScanning: () => void;
  stopScanning: () => void;
  getBLEState: () => void;
  addListener: (on: Events, callback: (e: any) => void) => any;
}

const startScanning = () => {
  return NativeModules.Beacon.startScanning(UUID);
};

const startBroadcast = async () => {
  const userUUID = await AsyncStorage.getItem('UUID');
  return NativeModules.Beacon.startBroadcast(userUUID);
};

const stopScanning = () => {
  return NativeModules.Beacon.stopScanning();
};

const stopBroadcast = () => {
  return NativeModules.Beacon.stopBroadcast();
};

const addListener = (on: Events, callback: (e: any) => void) => {
  const a = emitter.addListener(on, callback);
  return a;
};

export const BLE: IBLE = {
  ...NativeModules.Beacon,
  startBroadcast,
  startScanning,
  addListener,
  stopScanning,
  stopBroadcast,
};
