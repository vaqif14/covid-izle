import { createReducer, createActions } from 'reduxsauce'


const { Types, Creators } = createActions({
  btStatus: ['payload'],
})

export default Creators

const INITIAL_STATE = { bt_status: false }

const btStatus = (state, action) => {
  return {
    ...state,
    bt_status: action.payload.status === 'PoweredOn' ? true : false,
  }
}

const HANDLERS = {
  [Types.BT_STATUS]: btStatus,
}

export const reducer = createReducer(INITIAL_STATE, HANDLERS)
