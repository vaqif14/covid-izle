/**
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Alert, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import Card from "../Card";
import Bluethoot from "../../assets/img/bluetooth.png";
import AlertCircle from "../../assets/img/alert-circle.png";
var moment = require("moment");

import { BleManager } from "react-native-ble-plx";
class BluetoothStatus extends Component {
  constructor(props) {
    super(props);

    this.manager = new BleManager();
    this.subscription = null;

    this.state = {
      bluetoothPermission: false,
    };
  }
  componentDidMount() {
    this.subscription = this.manager.onStateChange((state) => {
      this.setState({
        bluetoothStatus: state,
      }),
        this.props.dispatch({
          type: "BT_STATUS",
          payload: { status: state },
        });
    }, true);
  }
  componentWillUnmount() {
    this.subscription;
  }
  enableBluetooth = () => {
    if (this.state.bluetoothStatus === "PoweredOff") {
      Alert.alert(
        "Bluetooth işləmir!",
        "Zəhmət olmasa tətbiqin işləməsi üçün Bluetooth-u açınız!",
        [
          {
            text: "Ləğv et",
            style: "cancel",
          },
          {
            text: "Qəbul et",
            onPress: () => {
              this.manager.enable();
              this.props.dispatch({
                type: "BT_STATUS",
                payload: { status: true },
              });
            },
          },
        ],
        { cancelable: true }
      );
    } else {
      alert("Aleady on");
    }
  };

  render() {
    const { bluetoothStatus } = this.state;
    return (
      <>
        {bluetoothStatus === "PoweredOff" && (
          <TouchableOpacity activeOpacity={1} onPress={this.enableBluetooth}>
            <Card
              imgSource={AlertCircle}
              icon={Bluethoot}
              bg="rgba(219, 44, 102, 0.9)"
              subBg="#DB2C66"
              title="Bluetooth işləmir!"
              subText="Zəhmət olmasa tətbiqin işləməsi üçün Bluetooth-u açınız!"
              date={moment().format("LL")}
              time={moment().format("HH:mm")}
            />
          </TouchableOpacity>
        )}
      </>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(
  null,
  mapDispatchToProps
)(BluetoothStatus);
