import {
  NativeModules,
  DeviceEventEmitter,
  NativeEventEmitter,
  Platform,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const UUID = 'c5263db3-131a-45b7-8dfb-d6add0071be0';

const emitter =
  Platform.OS !== 'android'
    ? new NativeEventEmitter(NativeModules.Beacon)
    : DeviceEventEmitter;
const startScanning = () => {
  return NativeModules.Beacon.startScanning(UUID);
};
const startBroadcast = async () => {
  const userUUID = await AsyncStorage.getItem('UUID');
  return NativeModules.Beacon.startBroadcast(userUUID);
};
const stopScanning = () => {
  return NativeModules.Beacon.stopScanning();
};
const stopBroadcast = () => {
  return NativeModules.Beacon.stopBroadcast();
};
const addListener = (on, callback) => {
  const a = emitter.addListener(on, callback);
  return a;
};
export const BLE = Object.assign(Object.assign({}, NativeModules.Beacon), {
  startBroadcast,
  startScanning,
  addListener,
  stopScanning,
  stopBroadcast,
});
