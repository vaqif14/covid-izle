import { Text as BaseText, H1 as BaseH1 } from 'native-base'
import styled from 'styled-components/native'

export const Paragraph = styled(BaseText)`
  margin-bottom: 15px;
`

export const H1 = styled(BaseH1)`
  margin-bottom: 15px;
`
export const TextError = styled(BaseText)`
  color: red;
  font-size: 12px;
  margin-top: -3px;
  margin-bottom: 7px;
`
