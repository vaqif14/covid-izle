import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { FormInput, FormValidationMessage } from 'react-native-elements'

class Input extends Component {
  _handleChange = (value) => {
    this.props.onChange(this.props.name, value)
  }

  _handleTouch = () => {
    this.props.onTouch(this.props.name)
  }
  render() {
    const { label, error, ...rest } = this.props
    return (
      <React.Fragment>
        {error && (
          <FormValidationMessage style={styles.error}>
            {error}
          </FormValidationMessage>
        )}
        <FormInput
          containerStyle={styles.formConatiner}
          inputStyle={{
            color: this.props.textColor,
          }}
          onChangeText={this._handleChange}
          onBlur={this._handleTouch}
          placeholder={label}
          {...rest}
        />
      </React.Fragment>
    )
  }
}

const styles = StyleSheet.create({
  formConatiner: {
    borderWidth: 1,
    borderColor: 'rgb(229,229,229)',
    paddingLeft: 15,
    width: 250,
    height: 40,
    borderStyle: 'solid',
    borderRadius: 50,
    marginBottom: 5,
  },

  error: {
    fontSize: 9,
  },
})
Input.defaultProps = {
  textColor: '#fff',
}
export default Input
