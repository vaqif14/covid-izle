import styled from 'styled-components/native'
import { Input } from 'native-base'

export const StyledInput = styled(Input)`
  margin-bottom: 10px;
  background-color: #eaf0f0;
  opacity: 0.4;
  border-radius: 4px;
  padding-left: 14px;
  padding-top: 6px;
  padding-bottom: 6px;
  height: 48px;
  font-family: 'Open Sans';

  font-size: 15px;
  line-height: 24px;
`
