import React, { useState, useEffect } from 'react'
import { Animated, Dimensions } from 'react-native'

export const FromBottom = (props) => {
  let { open } = props
  const height = Math.round(Dimensions.get('window').height - 100)
  const value = open ? height : 0
  let [marginTop] = useState(new Animated.Value(value))
  alert(open)

  useEffect(() => {
    Animated.timing(marginTop, {
      toValue: open ? 0 : height,
      duration: 500,
    }).start()
  }, [])

  return (
    <Animated.View
      style={{
        ...props.style,
        marginTop,
      }}
    >
      {props.children}
    </Animated.View>
  )
}
