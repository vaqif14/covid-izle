import React, { useState, useEffect } from 'react'
import { Animated } from 'react-native'

export const FadeIn = (props) => {
  const [opacity] = useState(new Animated.Value(0))

  useEffect(() => {
    Animated.timing(opacity, {
      toValue: 1,
      duration: 1000,
    }).start()
  }, [])

  return (
    <Animated.View
      style={{
        ...props.style,
        opacity: opacity,
      }}
    >
      {props.children}
    </Animated.View>
  )
}
