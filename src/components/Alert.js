import React, { useState } from 'react'
import * as Styled from './styles'
import Modal from 'react-native-modal'

export const Success = ({ componentId }) => {
  const [visible, setVisible] = useState(true)
  return (
    <Modal isVisible={visible}>
      <Styled.Alert>
        <Styled.Buttons>
          <Styled.Button
            color="#4EA982"
            onPress={() => {
              setVisible(false)
            }}
          >
            <Styled.Text color="#fff">Bağla</Styled.Text>
          </Styled.Button>
        </Styled.Buttons>
      </Styled.Alert>
    </Modal>
  )
}

export const Fail = () => {
  const [visible, setVisible] = useState(true)
  return (
    <Modal isVisible={visible}>
      <Styled.Alert>
        <Styled.AlertContent>
          <Styled.Icon
            type="Feather"
            name="alert-circle"
            color="#FF3D71"
            size="42px"
            bottom="5px"
          />
          <Styled.Text
            align="center"
            color="#FF3D71"
            size="28px"
            top="5px"
            bottom="5px"
          >
            Xəta!
          </Styled.Text>
          <Styled.Text align="center" color="#9D9F9F" bottom="30px">
            Xəta baş verdi. Zəhmət olmasa geriyə qayıdın və ya əməliyyatı
            təkrarlayın
          </Styled.Text>
        </Styled.AlertContent>
        <Styled.Buttons direction="row">
          <Styled.Button
            color="#DCE5E5"
            onPress={() => setVisible(false)}
            style={{ flex: 1, marginRight: 10 }}
          >
            <Styled.Text color="#515353" weight="bold">
              Bağla
            </Styled.Text>
          </Styled.Button>
          <Styled.Button
            color="#4EA982"
            onPress={() => setVisible(false)}
            style={{ flex: 1, marginLeft: 10 }}
          >
            <Styled.Text color="#fff" weight="bold">
              TƏKRAR
            </Styled.Text>
          </Styled.Button>
        </Styled.Buttons>
      </Styled.Alert>
    </Modal>
  )
}
