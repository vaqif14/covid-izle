import { NativeModules, DeviceEventEmitter } from 'react-native'
export const UUID = '848da4e8-0793-5813-8a36-7d7ee9d52914'
const emitter = DeviceEventEmitter
const startScanning = () => {
  return NativeModules.Beacon.startScanning(UUID)
}
const startBroadcast = () => {
  const userUUID = '848da4e8-0793-5813-8a36-7d7ee9d52914'
  return NativeModules.Beacon.startBroadcast(userUUID)
}
const stopScanning = () => {
  return NativeModules.Beacon.stopScanning()
}
const stopBroadcast = () => {
  return NativeModules.Beacon.stopBroadcast()
}
const addListener = (on, callback) => {
  return emitter.addListener(on, callback)
}
export const BLE = Object.assign(Object.assign({}, NativeModules.Beacon), {
  startBroadcast,
  startScanning,
  addListener,
  stopScanning,
  stopBroadcast,
})
