import React, { useEffect, useRef, useState } from 'react'
import { View, Text, Button, ScrollView, Alert, Platform } from 'react-native'
//@ts-ignore
import { BluetoothStatus } from 'react-native-bluetooth-status'
import { BLE } from './ble'

//@ts-ignore
import { useBluetoothStatus } from 'react-native-bluetooth-status'

let distance = []

async function enableBluetooth() {
  if (Platform.OS === 'android') {
    Alert.alert(
      'Enable Bluetooth',
      'Click Okay to go to settings',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Okay',
          onPress: () => BluetoothStatus.enable(),
        },
      ],
      { cancelable: true }
    )
  }
}

function Nearby() {
  const [isScaning, setIsScanning] = useState(false)
  const [state, setState] = useState('')
  let scanListener = useRef({ remove: () => {} })
  let bulkScanListener = useRef({ remove: () => {} })
  let emitterSuccessListener = useRef({ remove: () => {} })

  const [btStatus, isPending] = useBluetoothStatus()

  useEffect(() => {
    scanListener.current = BLE.addListener('onScanResult', (a) =>
      console.log(a)
    )

    emitterSuccessListener.current = BLE.addListener(
      'onBroadcastSuccess',
      () => {
        setIsScanning(true)
      }
    )

    if (!btStatus && !isPending) {
      enableBluetooth()
    }

    return () => {
      scanListener.current.remove()
      bulkScanListener.current.remove()
      emitterSuccessListener.current.remove()
    }
  }, [btStatus, isPending])

  const startEmittingAndReceiving = () => {
    BLE.startBroadcast()
    BLE.startScanning()
  }

  const stopEmitting = () => {
    setIsScanning(false)
    BLE.stopBroadcast()
    BLE.stopScanning()
  }

  return (
    <>
      <View>
        <View>
          <Text> </Text>
        </View>
        {isScaning ? (
          <Button title="ss" onPress={stopEmitting}>
            stopTracking
          </Button>
        ) : (
          <Button title="ss" onPress={startEmittingAndReceiving}>
            startTracking
          </Button>
        )}
        <ScrollView></ScrollView>
      </View>
    </>
  )
}

export { Nearby }
