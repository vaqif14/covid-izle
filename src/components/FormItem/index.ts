import { Item } from 'native-base'
import styled from 'styled-components'

export const FormItem = styled(Item)`
  margin-bottom: 10px;
  background-color: #eaf0f0;
  border-radius: 4px;
  padding-left: 14px;
  padding-top: 12px;
  padding-bottom: 12px;
`
