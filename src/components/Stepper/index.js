import React from 'react'

import StepIndicator from 'react-native-step-indicator'
import AntDesign from 'react-native-vector-icons/AntDesign'

const firstIndicatorStyles = {
  stepIndicatorSize: 30,
  currentStepIndicatorSize: 50,
  separatorStrokeWidth: 3,
  currentStepStrokeWidth: 0,
  separatorFinishedColor: '#5EC68B',
  separatorUnFinishedColor: '#DCE5E5',
  stepIndicatorFinishedColor: '#5EC68B',
  stepIndicatorUnFinishedColor: '#DCE5E5',
  stepIndicatorCurrentColor: '#5EC68B',
  width: '100%',
}

const getStepIndicatorIconConfig = ({ position, stepStatus }) => {
  const iconConfig = {
    name: 'feed',
    color: '#ffff',
    size: 30,
  }
  switch (position) {
    case 0: {
      iconConfig.name = 'adduser'
      break
    }
    case 1: {
      iconConfig.name = 'flag'
      break
    }
    default: {
      break
    }
  }
  return iconConfig
}

const renderStepIndicator = (params) => (
  <AntDesign {...getStepIndicatorIconConfig(params)} />
)
const Stepper = () => {
  return (
    <StepIndicator
      renderStepIndicator={renderStepIndicator}
      customStyles={firstIndicatorStyles}
      currentPosition={0}
      stepCount={2}
    />
  )
}

export default Stepper
