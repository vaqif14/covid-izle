import styled from 'styled-components/native'
import { Button } from 'native-base'

export const StyledButton = styled(Button)`
  margin-bottom: 15px;
  background-color: #4ea982;
  border-radius: 4px;
  height: 48px;
  justify-content: center;
`
