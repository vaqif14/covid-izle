import React from 'react'
import { View, StyleSheet, Image, Text } from 'react-native'
import Calendar from '../../assets/img/calendar.png'
import Clock from '../../assets/img/clock.png'
import CommonStyles from '../../styles/CommonStyles'

const Card = ({ imgSource, bg, subBg, title, subText, icon, time, date }) => {
  return (
    <View style={CommonStyles.mb2}>
      <View style={[styles.conatiner, { backgroundColor: bg }]}>
        {imgSource && (
          <Image style={{ position: 'absolute' }} source={imgSource} />
        )}
        <View style={{ flexDirection: 'row' }}>
          {icon && <Image source={icon} />}
          <Text style={styles.headingText}>{title}</Text>
        </View>
        <Text style={styles.subText}>{subText}</Text>
      </View>
      <View style={[styles.subContainer, , { backgroundColor: subBg }]}>
        <Text style={styles.iconText}>Sonuncu yenilənmə</Text>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Image source={Calendar} />
          <Text style={styles.iconText}>{date}</Text>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Image source={Clock} />
          <Text style={styles.iconText}>{time}</Text>
        </View>
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  conatiner: {
    backgroundColor: 'rgba(219, 139, 0, 0.8)',
    position: 'relative',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    minHeight: 140,
    padding: 16,
    shadowColor: 'rgba(219, 139, 0, 0.3)',
  },
  headingText: {
    color: '#fff',
    fontSize: 24,
    fontWeight: '700',
    lineHeight: 32,
    marginLeft: 10,
  },
  subText: {
    color: '#fff',
    fontSize: 15,
    fontWeight: '400',
    lineHeight: 20,
    marginTop: 10,
  },
  subContainer: {
    padding: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#DB8B00',
    position: 'relative',
    height: 36,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
  },
  iconText: {
    fontSize: 13,
    color: '#fff',
    marginLeft: 5,
  },
})
export default Card
