import React, { useEffect } from 'react'
import { Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import { Container, Spinner } from '../../components'
import { goToAuthScreen, goToHomeScreen } from '../../navigation'

export const INITIALIZING_SCREEN = {
  name: 'app.Initializing',
}

export const InitializingScreen = () => {
  const initializeApp = async () => {
    const user = await AsyncStorage.getItem('token')
    if (user) {
      await goToHomeScreen()
    } else {
      await goToAuthScreen()
    }
  }

  useEffect(() => {
    initializeApp().catch((error) => Alert.alert('error', `Xəta baş verdi`))
  }, [])

  return (
    <Container isCenter>
      <Spinner />
    </Container>
  )
}
