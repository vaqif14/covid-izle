import React from 'react'
import { Image } from 'react-native'
import LogoSm from '../assets/img/Logo_32.png'

const Tobar = () => <Image style={{ width: 150, height: 38 }} source={LogoSm} />

export default Tobar
