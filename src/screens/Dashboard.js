import React, { useEffect, useRef, useState } from "react";
import useInterval from "../modules/useInterval";
import { connect } from "react-redux";
import {
  ScrollView,
  TouchableOpacity,
  PermissionsAndroid,
  Text,
  View,
  Linking,
} from "react-native";
import { Button, Icon } from "native-base";
import NetInfo from "@react-native-community/netinfo";
import ActionButton from "react-native-action-button";
import { Navigation } from "react-native-navigation";
import Card from "../components/Card";
import ExpandedCard from "../components/ExpandedCard";
import { BLE } from "../components/ble/ble";
import BluetoothStatus from "../components/ble/enableBle";
import Notif from "../modules/notificationManager";
import RNAndroidLocationEnabler from "react-native-android-location-enabler";
import AsyncStorage from "@react-native-community/async-storage";
import useAsyncStorage from "../modules/useAsyncStorage";
import useAppState from "../modules/useAppState";
import CommonStyles from "../styles/CommonStyles";
import { encryptData, decryptData } from "../redux/userSevice/crypt";
import { getDistance } from "../components/ble/service";
import localization from "moment/locale/az";
import { goToAuthScreen } from "../navigation";
import axios from "axios";

import AlertCircleSm from "../assets/img/alert-circlesm.png";
import AlertCircleBg from "../assets/img/alert-circleBg.png";
import AlertImg from "../assets/img/alert-triangle.png";
import Radio from "../assets/img/radio.png";
import AllDone from "../assets/img/all-done.png";
import Bell from "../assets/img/bell.png";
import Bellsm from "../assets/img/bellsm.png";
import alertTrianglesm from "../assets/img/alert-trianglesm.png";
import alertTriangle from "../assets/img/alert-triangle.png";
import CheckMark from "../assets/img/checkmark-circle-2.png";
var moment = require("moment");

let lastChunk = [];

function Dashboard({ btStatus, componentId }) {
  const [isScaning, setIsScanning] = useState(false);
  const [network, setNetwork] = useState(false);
  const [location, setLocation] = useState(false);
  const [statusTime, setStatusTime] = useState(moment().format("HH:mm"));
  const [time, setTime] = useState(10000);
  const [timeCorrection, setTimeCorrection] = useState(0);
  const [sickIntersection, setSickIntersection] = useState(null);
  const [applicationState, setAppState] = useState(false);
  const [notificationTime, setNotificationTime] = useState(time + 30000);
  const [showMessage, setShowMessage] = useState({
    data: {
      messageType: "",
      messageContent: "",
      messageHeader: "",
      messageId: "",
      buttonPrimary: {},
      buttonSecondary: {},
      show: false,
    },
  });
  const [sickButton, setSickButton] = useState(false);
  const [getHeadChunkNumber, setHeadChunkNumber] = useState(1);
  const [getTailChunkNumber, setTailChunkNumber] = useAsyncStorage(
    "@tailChunkNumber"
  );
  // Set Global
  let headChunkNumber = getHeadChunkNumber;
  let tailChunkNumber = getTailChunkNumber || 1;
  //BLE listeners
  let scanListener = useRef({ remove: () => {} });
  let bulkScanListener = useRef({ remove: () => {} });
  let emitterSuccessListener = useRef({ remove: () => {} });

  useAppState({
    onForeground: () => setAppState(false),
    onBackground: () => setAppState(true),
  });

  const markChunkAsSentSuccess = async () => {
    if (headChunkNumber < tailChunkNumber) {
      await AsyncStorage.removeItem(`@data_${headChunkNumber}`);
      setHeadChunkNumber(1 + headChunkNumber);
    }
  };

  const getChunkToSendServer = async () => {
    while (headChunkNumber <= tailChunkNumber) {
      const isLastChunkNumber = headChunkNumber === tailChunkNumber; // create local copy of flag
      const result = await AsyncStorage.getItem(`@data_${headChunkNumber}`);
      if (result !== null) {
        return isLastChunkNumber &&
          (result.length < 256 || result.length > 1024)
          ? null // we return lastChunk data only for debug reasons, so let's limit it by some range of size.
          : result;
      }
      if (isLastChunkNumber) {
        break;
      }
      if (result !== null) return result;
      await markChunkAsSentSuccess();
    }
    return null;
  };

  const treatSickList = (sickList) => {
    if (!Array.isArray(sickList) || !sickList.length) {
      return;
    }

    let promises = [];
    for (
      var chunkNumber = headChunkNumber;
      chunkNumber <= tailChunkNumber;
      chunkNumber++
    ) {
      promises.push(
        AsyncStorage.getItem(`@data_${chunkNumber}`).then((encryptedData) => {
          const result = [];
          // lastChunk need be read from memory, not from storage
          const data =
            chunkNumber === tailChunkNumber
              ? lastChunk
              : decryptData(encryptedData);
          if (!Array.isArray(data)) {
            return result;
          }
          // enumerate one minute elements and find intersection with sickList
          data.forEach((elem) => {
            let resultElem = null;
            Object.entries(elem.data).forEach(([neighborId, scans]) => {
              if (
                0 <= sickList.findIndex((sick) => neighborId.startsWith(sick))
              ) {
                resultElem = resultElem || {
                  when: elem.when,
                  scans: elem.scans,
                  data: {},
                };
                resultElem.data[neighborId] = scans;
              }
            });
            if (resultElem) {
              result.push(resultElem);
            }
          });
          return result;
        })
      );
    }

    Promise.all(promises).then((data) => {
      data = [].concat(data);
      if (data.length) {
        setSickIntersection(encryptData(data));
      }
    });
  };
  const userStatus = async () => {
    if (!network) {
      return;
    }
    try {
      let token = await AsyncStorage.getItem("token");
      const sickResults = sickIntersection;
      let data = sickResults || (await getChunkToSendServer()) || "";
      const res = await axios.post(
        "https://covid.economy.gov.az/api/1/user/status",
        data,
        {
          headers: {
            "Content-Type": "text/plain; charset=utf-8",
            "X-Token": token,
          },
        }
      );
      if (res.status === 200) {
        // calc correction as fast as possible
        if (+res.data.timestamp > 0) {
          setTimeCorrection(res.data.timestamp * 1000 - Date.now());
        }

        sickResults
          ? setSickIntersection(null)
          : await markChunkAsSentSuccess();

        if (+res.data.repeatInterval > 0) {
          setTime(+res.data.repeatInterval * 1000);
        }
        if (res.data.showMessage) {
          const {
            showMessage: { id, type, content, header, buttons },
          } = res.data;
          setShowMessage({
            data: {
              messageId: id,
              messageType: type,
              messageContent: content,
              messageHeader: header,
              buttonPrimary: buttons[0],
              buttonSecondary: buttons[1],
              show: true,
            },
          });
        }

        if (res.data.showSickButton) {
          setSickButton(true);
        }

        treatSickList(res.data.sickList);
      }
    } catch (e) {
      if (e == "Error: Request failed with status code 401") {
        await AsyncStorage.clear();
        await goToAuthScreen();
      }
    }
  };

  const getNotification = async () => {
    let token = await AsyncStorage.getItem("token");
    try {
      const res = await axios.post(
        "https://covid.economy.gov.az/api/1/user/status",
        null,
        {
          headers: {
            "Content-Type": "text/plain; charset=utf-8",
            "X-Token": token,
          },
        }
      );
      if (res.status === 200) {
        // calc correction as fast as possible
        if (res.data.showMessage) {
          const {
            showMessage: { id, type, content, header, buttons },
          } = res.data;
          applicationState &&
            !applicationState &&
            setShowMessage({
              data: {
                messageId: id,
                messageType: type,
                messageContent: content,
                messageHeader: header,
                buttonPrimary: buttons[0],
                buttonSecondary: buttons[1],
                show: true,
              },
            });
        }
      }
    } catch (e) {
      console.log(e);
    }
  };
  useInterval(async () => {
    userStatus();
    setStatusTime(moment().format("HH:mm"));
  }, time);

  useInterval(async () => {
    getNotification();
  }, notificationTime);

  async function AddToStore(now, neighborId, macAddress) {
    const when = moment(now).format("MMDDTHHmm");
    if (macAddress) neighborId += "/" + macAddress;
    const lastItem = lastChunk.length && lastChunk[lastChunk.length - 1];
    let workItem =
      lastItem && lastItem.when === when
        ? lastItem
        : { when, scans: 1, data: {} };
    let scans = 1 + (workItem.data[neighborId] || 0);
    workItem.data[neighborId] = scans;
    if (workItem.scans < scans) workItem.scans = scans;
    if (workItem !== lastItem) {
      const encrypted = encryptData(lastChunk);
      console.log({ encrypted });
      await AsyncStorage.setItem("@data_" + tailChunkNumber, encrypted);

      if (encrypted.length > 64 * 1024) {
        setTailChunkNumber(tailChunkNumber + 1);
        lastChunk = [];
      }
      lastChunk.push(workItem);
    }
  }

  const insertRecord = (e) => {
    e.rssi === 0 && -1.0;
    const distance = getDistance(e.rssi, e.txPower || 59);
    if (distance <= 2.5) {
      AddToStore(Date.now() + timeCorrection, e.deviceId, e.macAddress);
    }
  };

  useEffect(() => {
    scanListener.current = BLE.addListener("onScanResult", insertRecord);

    bulkScanListener.current = BLE.addListener("onScanResultBulk", (e) => {
      e.forEach(insertRecord);
    });

    emitterSuccessListener.current = BLE.addListener(
      "onBroadcastSuccess",
      () => {
        setIsScanning(true);
      }
    );

    startEmittingAndReceiving();

    return () => {
      scanListener.current.remove();
      bulkScanListener.current.remove();
      emitterSuccessListener.current.remove();
    };
  }, [btStatus]);

  const enableLocation = async () => {
    const enable = RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
      interval: 1000,
      fastInterval: 500,
    })
      .then(() => {
        setLocation(true);
      })
      .catch((err) => {
        setLocation(false);
      });
    return enable;
  };
  const startEmittingAndReceiving = () => {
    BLE.startBroadcast();
    BLE.startScanning();
  };

  useEffect(() => {
    userStatus();
    NetInfo.addEventListener((state) => {
      setNetwork(state.isConnected);
    });
  }, [network]);

  sendMessageStatusAsync = async (id, data) => {
    try {
      let token = await AsyncStorage.getItem("token");
      await axios.post(
        "https://covid.economy.gov.az/api/1/message/status",
        { id: id, data: data },
        {
          headers: {
            "Content-Type": "application/json",
            "X-Token": token,
          },
        }
      );
    } catch (e) {}
  };
  locationPersmission = async () => {
    try {
      const response = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      );
      return response;
    } catch (err) {}
  };

  useEffect(() => {
    locationPersmission();
    enableLocation();
  }, []);

  return (
    <View style={CommonStyles.normalPage}>
      <Notif />
      <ScrollView style={{ padding: 20 }}>
        {sickButton && (
          <View style={{ width: "100%", marginBottom: 20 }}>
            <Button
              style={{
                height: 48,
                backgroundColor: "#FF3D71",
                borderRadius: 4,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text style={{ fontSize: 16, color: "#fff", fontWeight: "700" }}>
                ÖZÜMÜ PİS HİSS EDİRƏM
              </Text>
            </Button>
          </View>
        )}
        {!location && (
          <TouchableOpacity activeOpacity={1} onPress={enableLocation}>
            <Card
              imgSource={Bell}
              icon={Radio}
              bg="rgba(8, 69, 130, 0.9)"
              subBg="rgba(8, 69, 100, 1)"
              title="GPS aktiv edin!"
              subText="Cihazlar arasında məsafəni təyin etmək üçün zəhmət olmasa məkanı aktivləşdirin. Bu funksiya bluetooth-un işləməsi üçün vacibdir. Tətbiq sizin məkanınız haqqında heç bir məlumat toplamır."
              date={moment().format("LL")}
              time={statusTime}
            />
          </TouchableOpacity>
        )}
        <BluetoothStatus />
        {!network && (
          <Card
            imgSource={AlertImg}
            icon={Radio}
            bg="rgba(219, 139, 0, 0.9)"
            subBg="#DB8B00"
            title="Şəbəkə yoxdur!"
            subText="Serverlə əlaqə kəsilmişdir. Zəhmət olmasa internet bağlantınızı yoxlayasınız."
            date={moment().format("LL")}
            time={statusTime}
          />
        )}
        {network && btStatus && location && !showMessage.data.show && (
          <Card
            imgSource={AllDone}
            icon={CheckMark}
            bg="rgba(78, 169, 130, 0.9)"
            subBg="#4EA982"
            title="Hər şey qaydasındadır"
            subText="Zəhmət olmasa tətbiqi hər zaman işlək vəziyyətdə saxlayın! Xüsusilə iclaslarda, ictimai nəqliyyatda və ictimai yerlərdə. Proqramı enerjiyə qənaət rejimində işləyənlərin siyahısına telefonun ayarlarıdan əlavə edə bilərsiniz."
            date={moment()
              .locale("az", localization)
              .format("LL")
              .toUpperCase()}
            time={statusTime}
          />
        )}
        {showMessage.data.show && (
          <ExpandedCard
            imgSource={
              showMessage.data.messageType == "info"
                ? Bell
                : showMessage.data.messageType == "urgent"
                ? AlertCircleBg
                : alertTriangle
            }
            icon={
              showMessage.data.messageType == "info"
                ? Bellsm
                : showMessage.data.messageType == "urgent"
                ? AlertCircleSm
                : alertTrianglesm
            }
            bg={
              showMessage.data.messageType == "info"
                ? "rgba(0, 149, 255, 0.9)"
                : showMessage.data.messageType == "urgent"
                ? "rgba(219, 44, 102, 0.9)"
                : "#FFAA00"
            }
            title={showMessage.data.messageHeader}
            description={showMessage.data.messageContent}
            primaryIcon={
              showMessage.data.buttonPrimary.label == "103-ə zəng etmək"
            }
            primaryBtnText={showMessage.data.buttonPrimary.label}
            primaryBtnCallback={() => {
              setShowMessage({
                data: {
                  messageType: "",
                  messageContent: "",
                  messageHeader: "",
                  messageId: "",
                  buttonPrimary: {},
                  buttonSecondary: {},
                  show: false,
                },
              });
              sendMessageStatusAsync(
                showMessage.data.messageId,
                showMessage.data.buttonPrimary.data
              );
            }}
            secondaryIcon={
              showMessage.data.buttonSecondary.label == "103-ə zəng etmək"
            }
            secondaryBtnText={showMessage.data.buttonSecondary.label}
            secondaryBtnCallback={() => {
              sendMessageStatusAsync(
                showMessage.data.messageId,
                showMessage.data.buttonSecondary.data
              );
              if (
                showMessage.data.buttonSecondary.label == "103-ə zəng etmək"
              ) {
                Linking.openURL(`tel:${showMessage.data.buttonSecondary.data}`);
              }
            }}
            time={moment().format("HH:MM")}
          />
        )}
      </ScrollView>
      <ActionButton
        buttonColor="#5EC68B"
        renderIcon={() => (
          <Icon name="share" size={20} style={{ color: "#fff" }} />
        )}
        onPress={async () =>
          await Navigation.push(componentId, {
            component: {
              name: "Share",
              options: {
                topBar: {
                  visible: true,
                  title: { text: "Tətbiqi paylaş" },
                  background: {
                    color: "#D8FADD",
                    translucent: true,
                  },
                },
              },
            },
          })
        }
      />
    </View>
  );
}
const mapStateToProps = (state) => ({
  btStatus: state.btStatus.bt_status,
});

export default connect(
  mapStateToProps,
  null
)(Dashboard);
