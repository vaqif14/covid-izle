import React, { Component } from 'react'
import AsyncStorage from '@react-native-community/async-storage'
import { Image, TouchableOpacity, StyleSheet } from 'react-native'
import Logout from '../assets/img/logOut.png'
import { goToAuthScreen } from '../navigation'

import { connect } from 'react-redux'

export const TopbarRight = ({ dispatch }) => {
  const logOut = async () => {
    dispatch({ type: 'RESET_USERS' })
    dispatch({ type: 'RESET_STORED_USER' })
    dispatch({ type: 'RESET_OFFLINE_DATA' })
    dispatch({ type: 'RESET_REG_USER' })
    await AsyncStorage.removeItem('token')
    await AsyncStorage.removeItem('uuid')
    await goToAuthScreen()
  }
  return (
    <TouchableOpacity onPress={logOut} style={styles.container}>
      <Image style={{ width: 30, height: 30 }} source={Logout} />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

const mapDispatchToProps = (dispatch) => ({
  dispatch,
})

export default connect(null, mapDispatchToProps)(TopbarRight)
