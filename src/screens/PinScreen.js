import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage'
import Logo from '../assets/img/logo.png'
import SmoothPinCodeInput from 'react-native-smooth-pincode-input'
import CountDown from 'react-native-countdown-component'
import { goToAuthScreen } from '../navigation'
import * as Styled from '../styles/Styled'

class PinScreen extends Component {
  state = {
    pinCheck: '',
    countdown: true,
    pinCode: '',
  }
  pinInput = React.createRef()

  _setCode = (code) => {
    this.setState({ pinCode: code })
  }

  btnPress = () => {
    const {
      regUser: { error },
    } = this.props.state
    this.props.dispatch({
      type: 'GET_SMS',
      sms: this.state.pinCode,
    })
  }

  countdownFinished = async () => {
    this.setState({ countdown: !this.state.countdown })
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.state.regUser.error) {
      this.pinInput.current.shake().then(() => this.setState({ pinCheck: '' }))
    }
  }
  render() {
    const { pinCheck, countdown } = this.state

    return (
      <Styled.ScrollView bg="white" contentContainerStyle={{ flexGrow: 1 }}>
        {/* Header */}
        <Styled.View
          bg="#F7F8F8"
          height="146px"
          alignItems="center"
          justifyContent="center"
        >
          <Styled.Image
            source={Logo}
            width="64px"
            height="64px"
            resizeMode="contain"
          />
          <Styled.Text
            color="#408D77"
            fontSize="18px"
            fontWeight="bold"
            mt="20px"
          >
            COVID izlə
          </Styled.Text>
        </Styled.View>

        {/* Stepper */}
        <Styled.View bg="#DCE5E5" position="relative" width="100%" height="2px">
          <Styled.View
            position="absolute"
            width="88%"
            height="4px"
            bg="#5EC68B"
            top="-1px"
          ></Styled.View>
          <Styled.Icon
            type="Feather"
            name="user-plus"
            position="absolute"
            left="10%"
            top="-18px"
            width="36px"
            height="36px"
            borderRadius="36px"
            bg="#5EC68B"
            color="#fff"
            fontSize="18px"
            textAlign="center"
            lineHeight="36px"
          ></Styled.Icon>
          <Styled.Icon
            type="Feather"
            name="flag"
            position="absolute"
            right="10%"
            top="-18px"
            width="36px"
            height="36px"
            borderRadius="36px"
            bg="#5EC68B"
            color="#fff"
            fontSize="18px"
            textAlign="center"
            lineHeight="36px"
          ></Styled.Icon>
        </Styled.View>

        {/* Help text */}
        <Styled.View
          px="15px"
          pb="20px"
          pt="30px"
          borderBottomWidth="1px"
          borderColor="#ddd"
        >
          <Styled.Text
            color="#9D9F9F"
            fontSize="16px"
            fontWeight="bold"
            mb="5px"
          >
            Koronavirusu dayandırmağa kömək edin!
          </Styled.Text>
          <Styled.Text fontSize="13px" lineHeight="20px" color="#383838">
            Təsdiq kodu SMS vasitəsi ilə, göstərdiyiniz telefon nömrəsinə
            göndərilmişdir. Qeydiyyatı başa çatdırmaq üçün kodu daxil edin.
          </Styled.Text>
        </Styled.View>

        {/* Content */}
        <Styled.View flexGrow="1" p="15px">
          {/* Code */}
          <Styled.View flexGrow="1" alignItems="center" py="20px">
            <Styled.Text color="#9D9F9F">
              Telefonunuza gələn kodu daxil edin
            </Styled.Text>
            <SmoothPinCodeInput
              ref={this.pinInput}
              cellStyle={styles.regcellStyle}
              cellStyleFocused={styles.regcellStyleFocused}
              textStyle={styles.regtextStyleFocused}
              autoFocus={true}
              cellSize={48}
              codeLength={6}
              value={pinCheck}
              onTextChange={(pinCheck) => this.setState({ pinCheck })}
              onFulfill={this._setCode}
            />

            <Styled.Text color="#9D9F9F" mt="40px" mb="10px">
              Kodu almadınız?
            </Styled.Text>
            {countdown ? (
              <CountDown
                until={60}
                onFinish={() => this.countdownFinished()}
                size={15}
                timeToShow={['M', 'S']}
                timeLabels={{ m: null, s: null }}
              />
            ) : (
              <Styled.Button
                bg="#DCE5E5"
                py="13px"
                px="20px"
                borderRadius="4px"
                onPress={() => {
                  this.setState({ countdown: true })
                  this.props.dispatch({ type: 'SEND_AGAIN' })
                }}
              >
                <Styled.Text color="#515353" fontFamily="Roboto-Medium">
                  YENİDƏN GÖNDƏRMƏK
                </Styled.Text>
              </Styled.Button>
            )}
          </Styled.View>

          {/* Button */}
          <Styled.View flexDirection="row">
            {/* Back */}
            <Styled.Button
              flex="1"
              mr="10px"
              bg="#DCE5E5"
              borderRadius="4px"
              height="48px"
              alignItems="center"
              justifyContent="center"
              onPress={async () => await goToAuthScreen()}
            >
              <Styled.Text fontSize="16px" fontWeight="bold" color="#515353">
                GERİYƏ
              </Styled.Text>
            </Styled.Button>

            {/* Register */}
            <Styled.Button
              flex="1"
              ml="10px"
              bg="#4EA982"
              borderRadius="4px"
              height="48px"
              alignItems="center"
              justifyContent="center"
              onPress={this.btnPress}
            >
              <Styled.Text fontSize="16px" fontWeight="bold" color="#fff">
                QEYDİYYAT
              </Styled.Text>
            </Styled.Button>
          </Styled.View>
        </Styled.View>
      </Styled.ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  regcellStyle: {
    borderWidth: 1,
    borderColor: '#EAF0F0',
    borderRadius: 4,
    backgroundColor: '#EAF0F0',
    marginTop: 20,
  },
  regcellStyleFocused: {
    borderWidth: 2,
    borderColor: '#4EA982',
  },
  regtextStyleFocused: {
    fontSize: 28,
    color: '#383838',
  },
})

const mapStateToProps = (state) => ({ state })

function mapDispatchToProps(dispatch) {
  return { dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(PinScreen)
