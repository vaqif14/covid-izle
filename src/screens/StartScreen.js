import React, { Component } from 'react'
import { Navigation } from 'react-native-navigation'
import { connect } from 'react-redux'
import Logo from '../assets/img/logo.png'
import { RadioButton } from 'react-native-paper'
import { Formik } from 'formik'
import * as Yup from 'yup'
import * as Styled from '../styles/Styled'
import { Spinner } from 'native-base'

class StartScreen extends Component {
  constructor() {
    super()
    this.state = {
      radioCheck: '',
      gender: '',
      age: '',
      phone: '',
    }
  }

  render() {
    const { radioCheck } = this.state
    return (
      <Styled.ScrollView bg="white" contentContainerStyle={{ flexGrow: 1 }}>
        {/* Header */}
        <Styled.View
          bg="#F7F8F8"
          height="146px"
          alignItems="center"
          justifyContent="center"
        >
          <Styled.Image
            source={Logo}
            width="64px"
            height="64px"
            resizeMode="contain"
          />
          <Styled.Text
            color="#408D77"
            fontSize="18px"
            fontWeight="bold"
            mt="20px"
          >
            COVID izlə
          </Styled.Text>
        </Styled.View>
        {/* Stepper */}
        <Styled.View bg="#DCE5E5" position="relative" width="100%" height="2px">
          <Styled.View
            position="absolute"
            width="11%"
            height="4px"
            bg="#5EC68B"
            top="-1px"
          ></Styled.View>
          <Styled.Icon
            type="Feather"
            name="user-plus"
            position="absolute"
            left="10%"
            top="-18px"
            width="36px"
            height="36px"
            borderRadius="36px"
            bg="#5EC68B"
            color="#fff"
            fontSize="18px"
            textAlign="center"
            lineHeight="36px"
          ></Styled.Icon>
          <Styled.Icon
            type="Feather"
            name="flag"
            position="absolute"
            right="10%"
            top="-18px"
            width="36px"
            height="36px"
            borderRadius="36px"
            bg="#DDD"
            color="#fff"
            fontSize="18px"
            textAlign="center"
            lineHeight="36px"
          ></Styled.Icon>
        </Styled.View>

        {/* Help text */}
        <Styled.View
          px="15px"
          pb="20px"
          pt="30px"
          borderBottomWidth="1px"
          borderColor="#ddd"
        >
          <Styled.Text
            color="#9D9F9F"
            fontSize="16px"
            fontWeight="bold"
            mb="5px"
          >
            Koronavirusu dayandırmağa kömək edin!
          </Styled.Text>
          <Styled.Text fontSize="13px" lineHeight="20px" color="#383838">
            Əgər təsadüfən infeksiyanın daşıyıcısı ilə əlaqəniz olubsa, Səhiyyə
            Nazirliyi sizinlə əlaqə quracaq və siz dərhal testdən keçməyə dəvət
            olunacaqsınız!
          </Styled.Text>
        </Styled.View>

        {/* Form */}
        <Formik
          validationSchema={Yup.object().shape({
            age: Yup.string()
              .max(3, 'Yaşınız düzgün daxil edilməyib')
              .required('Yaşınızı daxil edin'),
            phone: Yup.string()
              .max(9, 'Nömrə düzgün daxil edilməyib')
              .required('Telefon nömrənizi daxil edin'),
          })}
          initialValues={{ age: '', phone: '' }}
          onSubmit={(values) => {
            this.props.dispatch({
              type: 'REG_USER',
              payload: {
                id: this.props.componentId,
                gender: this.state.radioCheck,
                age: values.age,
                number: values.phone,
                loading: true,
              },
            })
          }}
        >
          {({
            handleChange,
            setFieldTouched,
            touched,
            handleSubmit,
            values,
            errors,
          }) => (
            <Styled.View p="15px" flexGrow="1">
              <Styled.View flexGrow="1" mb="15px">
                {/* Gender */}
                <Styled.Text
                  fontSize="14px"
                  fontFamily="Roboto-Medium"
                  color="#9D9F9F"
                  mb="5px"
                >
                  Cinsiniz
                </Styled.Text>
                <Styled.View flexDirection="row" ml="-7px">
                  {/* Man */}
                  <Styled.Button
                    flexDirection="row"
                    alignItems="center"
                    flex="1"
                    onPress={() => this.setState({ radioCheck: 1 })}
                  >
                    <RadioButton
                      value="man"
                      color="#5EC68B"
                      selectedColor="#5EC68B"
                      status={radioCheck === 1 ? 'checked' : 'unchecked'}
                      onPress={() => this.setState({ radioCheck: 1 })}
                    />
                    <Styled.Text fontSize="13px" fontFamily="Roboto-Medium">
                      Kişi
                    </Styled.Text>
                  </Styled.Button>

                  {/* Woman */}
                  <Styled.Button
                    flexDirection="row"
                    alignItems="center"
                    flex="1"
                    onPress={() => this.setState({ radioCheck: 0 })}
                  >
                    <RadioButton
                      value="woman"
                      color="#5EC68B"
                      selectedColor="#5EC68B"
                      status={radioCheck === 0 ? 'checked' : 'unchecked'}
                      onPress={() => this.setState({ radioCheck: 0 })}
                    />
                    <Styled.Text fontSize="13px" fontFamily="Roboto-Medium">
                      Qadın
                    </Styled.Text>
                  </Styled.Button>
                </Styled.View>

                {/* Age */}
                <Styled.Text
                  fontSize="14px"
                  fontFamily="Roboto-Medium"
                  color="#9D9F9F"
                  mt="15px"
                >
                  Yaşınız
                </Styled.Text>
                {touched.age && errors.age && (
                  <Styled.Text color="red" fontSize="12px">
                    {errors.age}
                  </Styled.Text>
                )}
                <Styled.Input
                  bg="#EAF0F0"
                  borderRadius="4px"
                  width="130px"
                  height="48px"
                  flex="none"
                  px="10px"
                  mt="10px"
                  keyboardType={'phone-pad'}
                  onChangeText={handleChange('age')}
                  onBlur={() => setFieldTouched('age')}
                />

                {/* Phone */}
                <Styled.Text
                  fontSize="14px"
                  fontFamily="Roboto-Medium"
                  color="#9D9F9F"
                  mt="15px"
                >
                  Mobil nömrəniz
                </Styled.Text>
                {touched.phone && errors.phone && (
                  <Styled.Text color="red" fontSize="12px">
                    {errors.phone}
                  </Styled.Text>
                )}
                <Styled.View flexDirection="row" mt="10px">
                  <Styled.Text
                    height="48px"
                    lineHeight="48px"
                    px="20px"
                    fontSize="16px"
                    color="#fff"
                    bg="#4EA982"
                    borderTopLeftRadius="4px"
                    borderBottomLeftRadius="4px"
                  >
                    +994
                  </Styled.Text>
                  <Styled.Input
                    bg="#EAF0F0"
                    px="15px"
                    borderTopRightRadius="4px"
                    borderBottomRightRadius="4px"
                    height="48px"
                    placeholder="50  ___  __  __"
                    keyboardType={'phone-pad'}
                    onChangeText={handleChange('phone')}
                    onBlur={() => setFieldTouched('phone')}
                  />
                </Styled.View>
                <Styled.Text mt="20px" color="#383838">
                  Tətbiq şəxsi və ya geolokasiya məlumatlarını toplamır!
                </Styled.Text>
              </Styled.View>

              {/* Button */}
              <Styled.Button
                bg="#4EA982"
                borderRadius="4px"
                height="48px"
                alignItems="center"
                justifyContent="center"
                onPress={handleSubmit}
              >
                {this.props.state.regUser.loading ? (
                  <Spinner color="#fff" />
                ) : (
                  <Styled.Text fontSize="16px" fontWeight="bold" color="#fff">
                    QOŞULMAQ
                  </Styled.Text>
                )}
              </Styled.Button>
            </Styled.View>
          )}
        </Formik>
      </Styled.ScrollView>
    )
  }
}

const mapStateToProps = (state) => ({ state })

function mapDispatchToProps(dispatch) {
  return { dispatch }
}

export default connect(mapStateToProps, mapDispatchToProps)(StartScreen)
