import React from 'react';
import {Dimensions} from 'react-native';
import styled from 'styled-components/native';
import is from 'styled-is';

const height = Math.round(Dimensions.get('window').height - 25);

export const Container = styled.View`
  flex: 1;
  background-color: #fff;
  padding: 0 10px;
`;

export const View = styled.View`
  flex: 1;
`;

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const Header = styled.View`
  padding: 10px 5px 15px;
  /* border-bottom-width: 1px; */
  /* border-color: #ddd; */
  position: relative;
`;
export const Title = styled.Text`
  font-size: 26px;
  font-weight: bold;
  flex: 1;
`;

export const SelectAll = styled.Text`
  font-size: 16px;
  align-self: flex-end;
  color: #1e90ff;
  padding: 5px 10px;
`;

export const SearchInput = styled.TextInput`
  padding: 8px 12px;
  background-color: #eee;
  border-radius: 10px;
  font-size: 15px;
  margin-top: 13px;
`;

export const List = styled.ScrollView`
  margin-bottom: 15px;
`;

export const Item = styled.TouchableOpacity`
  padding: 13px 0;
  flex-direction: row;
  align-items: center;
  ${is('border')`
		border-bottom-width: 1px;
		border-color: #ddd;
	`}
`;

export const ItemText = styled.Text`
  font-size: 16px;
  margin-left: 20px;
`;

export const TouchableOpacity = styled.TouchableOpacity`
  position: absolute;
  right: 0;
  top: 10px;
  width: 40px;
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 10;
`;
export const XText = styled.Text`
  font-size: 36px;
  color: #333;
`;
export const X = ({text, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <XText>{text}</XText>
    </TouchableOpacity>
  );
};
