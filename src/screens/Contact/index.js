import React, { useEffect, useState } from "react";
import { useNavigationComponentDidDisappear } from "react-native-navigation-hooks";
import { View, Container, Header, SearchInput } from "./style";
import { Navigation } from "react-native-navigation";
import { connect } from "react-redux";
import { PermissionsAndroid } from "react-native";
import { Button } from "../../components";
import { CheckBox, ListItem, Left } from "native-base";
import { Text, FlatList } from "react-native";
import Contacts from "react-native-contacts";

let arr = [];
let check = [];
let send = [];

function ContactScreen({ componentId, dispatch }) {
  const [searchData, setSearch] = useState([]);
  const [selected, setSelected] = useState([]);
  const [checked, setChecked] = useState([]);

  async function getContact() {
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
      title: "Contacts",
      message: "This app would like to view your contacts.",
      buttonPositive: "Please accept bare mortal",
    }).then(() => {
      Contacts.getAll((err, contactData) => {
        if (err === "denied") {
          // error
        } else {
          if (arr.length !== contactData.length) {
            for (let i = 0; i < contactData.length; i++) {
              arr.push(contactData[i]);
            }
          }
          setSearch(arr);
        }
      });
    });
  }
  useEffect(() => {
    getContact();
  }, []);
  () => arr.length !== searchData.length && setSearch(arr);
  useNavigationComponentDidDisappear(() => {
    setSearch([]);
    setChecked([]);
    check = [];
  }, []);

  const Search = (text) => {
    Contacts.getContactsMatchingString(text, (err, result) => {
      setSearch(result);
    });
  };
  const onSelect = (item) => {
    if (checked.includes(item.recordID)) {
      delete check[checked.indexOf(item.recordID)];
    } else {
      check.push(item.recordID);
    }

    setChecked(check);
    setSelected([...selected, item]);
  };

  let dublicateContacts = () =>
    selected.filter((item) =>
      checked.includes(item.recordID)
        ? selected.indexOf(item.recordID) != selected.recordID
        : selected.phone !== item.phone
    );

  const Submit = () => {
    let data = [...new Set(dublicateContacts())];

    data.forEach((user) => {
      let number = user.phoneNumbers[0].number;
      number = number.replace(/\D/g, "");
      const zona = number.substr(0, 3);
      const zero = number.substr(0, 1);
      if (zona == "994") {
        number = number.substr(3);
      } else if (zero == "0") {
        number = number.substr(1);
      }
      if (isNaN(number) || number.length != 9) {
        return false;
      }
      send.push({
        name: user.givenName + " " + user.familyName,
        phone: number,
      });
    });
    dispatch({
      type: "SHARED_USERS",
      users: send,
    });
    setSearch([]);
    setChecked([]);
    check = [];
    send = [];
    Navigation.pop(componentId);
  };
  return (
    <View>
      <Container isCenter>
        <Header>
          <SearchInput
            placeholder="Axtar"
            onChangeText={(val) => Search(val)}
          />
        </Header>

        <FlatList
          style={{ padding: 0, margin: 0 }}
          data={searchData}
          renderItem={({ item, index }) =>
            item.phoneNumbers.length >= 1 && (
              <ListItem
                style={{ padding: 0, margin: 0 }}
                onPress={() => onSelect(item)}
                key={index}
              >
                <CheckBox
                  onPress={() => onSelect(item)}
                  checked={checked.includes(item.recordID)}
                />
                <Left
                  style={{
                    marginLeft: 10,
                    flexDirection: "column",
                    fontFamily: "OpenSans-Regular",
                  }}
                >
                  <Text
                    style={{ color: "#555", fontSize: 15, marginBottom: 5 }}
                  >
                    {item.givenName + " " + item.familyName}
                  </Text>
                  <Text style={{ color: "gray", fontSize: 11 }}>
                    {item.phoneNumbers[0].number}
                  </Text>
                </Left>
              </ListItem>
            )
          }
          keyExtractor={(item) => item.recordID}
        />

        <Button onPress={() => Submit()}>Əlavə et</Button>
      </Container>
    </View>
  );
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}
export default connect(
  null,
  mapDispatchToProps
)(ContactScreen);
