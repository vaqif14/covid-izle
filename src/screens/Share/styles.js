import styled from "styled-components/native";
import { Button as BaseButton, Icon as BaseIcon } from "native-base";
import is from "styled-is";

export const Container = styled.View`
  flex: 1;
  background-color: #fff;
`;

/** Header */
export const Header = styled.View`
  overflow: hidden;
  padding: 20px 0;
`;

export const HeaderOverlay = styled.View`
  position: absolute;
  background-color: rgba(216, 250, 221, 0.8);
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  z-index: 1;
`;

export const HeaderContent = styled.View`
  position: relative;
  z-index: 2;
  padding: 0 15px;
`;

export const HeaderBack = styled.TouchableOpacity`
  flex-direction: row;
  margin-top: 10px;
  align-items: center;
`;

export const HeaderBackText = styled.Text`
  color: #515353;
  font-weight: bold;
  font-size: 18px;
  margin-left: 10px;
`;

export const HeaderImage = styled.Image`
  position: absolute;
  width: 100%;
  height: 120%;
`;

export const HeaderTitle = styled.Text`
  font-weight: bold;
  font-size: 20px;
  line-height: 32px;
  text-align: center;
  color: #33736b;
  padding: 10px 0;
`;

/** Content */
export const Content = styled.View`
  flex: 1;
  background-color: #ffffff;
`;

export const Empty = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 0 20px;
`;

export const List = styled.ScrollView`
  padding: 0 15px;
`;

export const ListItem = styled.View`
  flex-direction: row;
  align-items: center;
  border-bottom-width: 1px;
  border-bottom-color: #dce5e5;
  padding: 5px 0;
`;

export const ListItemLeft = styled.View`
  flex: 1;
`;

/** Footer */
export const Footer = styled.View`
  padding: 15px 0 5px;
  border-top-width: 1px;
  border-color: #dce5e5;
`;

export const Buttons = styled.View`
  ${is("direction")` flex-direction: ${(props) => props.direction} `}
  padding: 0 15px;
`;

/** Alert */
export const Alert = styled.View`
  background-color: #fff;
  border-radius: 4px;
`;

export const AlertContent = styled.View`
  background-color: #fff;
  padding: 20px 20px 10px;
  border-radius: 4px;
  align-items: center;
  justify-content: center;
`;

/** Common Components */
export const Text = styled.Text`
	${is("color")` color: ${(props) => props.color} `}
	${is("size")` font-size: ${(props) => props.size} `}
	${is("weight")` font-weight: ${(props) => props.weight} `}
	${is("align")` text-align: ${(props) => props.align} `}
	${is("margin")` margin: ${(props) => props.margin} `}
	${is("top")` margin-top: ${(props) => props.top} `}
	${is("bottom")` margin-bottom: ${(props) => props.bottom} `}
	${is("left")` margin-left: ${(props) => props.left} `}
	${is("right")` margin-right: ${(props) => props.right} `}
`;

export const Icon = styled(BaseIcon)`
	${is("color")` color: ${(props) => props.color} `}
	${is("size")` font-size: ${(props) => props.size} `}
	${is("margin")` margin: ${(props) => props.margin} `}
	${is("top")` margin-top: ${(props) => props.top} `}
	${is("bottom")` margin-bottom: ${(props) => props.bottom} `}
	${is("left")` margin-left: ${(props) => props.left} `}
	${is("right")` margin-right: ${(props) => props.right} `}
`;

export const Button = styled(BaseButton)`
  border-radius: 4px;
  justify-content: center;
  align-items: center;
  margin-bottom: 10px;
  padding: 0 15px;
  height: 48px;
  /* flex: 1; */
  ${is("color")` background-color: ${(props) => props.color} `}
`;
