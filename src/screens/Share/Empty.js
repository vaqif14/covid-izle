import React from 'react';
import * as Styled from './styles';

const Empty = () => {
  return (
    <Styled.Empty>
      <Styled.Icon
        type="Feather"
        name="phone-missed"
        color="#9D9F9F"
        size="20px"
        bottom="5px"
      />
      <Styled.Text align="center" color="#9D9F9F">
        Heç bir kontakt seçilməyib!
      </Styled.Text>
      <Styled.Text align="center" color="#9D9F9F">
        Kontakt əlavə etmək üçün zəhmət olmasa aşağıdakı düyməyə basın
      </Styled.Text>
    </Styled.Empty>
  );
};

export default Empty;
