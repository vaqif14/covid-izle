import React from "react";
import { Navigation } from "react-native-navigation";
import { useNavigationComponentDidDisappear } from "react-native-navigation-hooks";

import HeaderBg from "../../assets/img/share-bg.png";
import { connect } from "react-redux";
import * as Styled from "./styles";

// Share screens
import Empty from "./Empty";
import List from "./List";
import { Success, Fail } from "./Alert";

function ShareIndex({ dispatch, name, success, error, componentId }) {
  const showContact = () => {
    Navigation.push(componentId, {
      component: {
        name: "ContactScreen",
        options: {
          topBar: {
            visible: true,
            title: { text: "Kontakt seç" },
            background: {
              color: "#D8FADD",
              translucent: true,
            },
          },
        },
      },
    });
  };
  useNavigationComponentDidDisappear((e) => {
    dispatch({
      type: "SHARED_USERS",
      users: "",
    });
    dispatch({
      type: "SUCCESS_CONTACT",
      success: false,
      error: false,
    });
  }, componentId);
  return (
    <Styled.Container>
      {/* Header */}
      <Styled.Header>
        <Styled.HeaderOverlay />
        <Styled.HeaderImage source={HeaderBg} />
        <Styled.HeaderContent>
          <Styled.HeaderTitle>
            Ailənizə və dostlarınıza koronavirusun qarşısını almağa kömək edin
          </Styled.HeaderTitle>
          <Styled.Text color="#515353" textAlign="justify">
            "Tətbiqi paylaşın" düyməsini tıkladığınız zaman, proqram
            telefonunuzdakı əlaqə kitabçasına girişini təmin etməyinizi xahiş
            edəcəkdir. Siz əlaqə kitabçasından ailənizi və dostlarınızı
            seçdikdən sonra proqram tətbiqi SMS dəvətnamə hazırlayaraq onları
            sizin seçdiyiniz insanlara göndərəcəkdir. SMS xidməti bizim
            tərəfimizdən təmin ediləcəyi üçün, sizin balansınızdan heç bir
            məbləğ silinməyəcəkdir.
          </Styled.Text>
        </Styled.HeaderContent>
      </Styled.Header>

      {/* Content */}
      <Styled.Content>
        {name.length >= 1 ? <List /> : <Empty />}
        {success && <Success />}
        {error && <Fail />}
      </Styled.Content>

      {/* Footer */}
      <Styled.Footer>
        <Styled.Buttons>
          <Styled.Button onPress={() => showContact()} color="#0095FF">
            <Styled.Text color="white" weight="600">
              KONTAKTLARI SEÇİN
            </Styled.Text>
          </Styled.Button>
          <Styled.Button
            onPress={() =>
              name.length >= 1 && dispatch({ type: "SHARED_USERS_CONFIRM" })
            }
            color={name.length >= 1 ? "#0095FF" : "#ace3cc"}
          >
            <Styled.Text color="white" weight="600">
              TƏTBİQİ PAYLAŞ
            </Styled.Text>
          </Styled.Button>
        </Styled.Buttons>
      </Styled.Footer>
    </Styled.Container>
  );
}

const mapStateToProps = (state) => ({
  name: state.shareContacs.users,
  success: state.shareContacs.success,
  error: state.shareContacs.error,
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShareIndex);
