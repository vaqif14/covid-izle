import React, {memo} from 'react';
import {TouchableOpacity} from 'react-native';
import * as Styled from './styles';
import {connect} from 'react-redux';
import {compose} from 'redux';

const List = ({name, dispatch}) => {
  return (
    <React.Fragment>
      <Styled.Text color="#515353" size="16px" margin="15px">
        Seçdiyiniz kontaktlar
      </Styled.Text>
      <Styled.List>
        {name.length >= 1 &&
          name.map((item, i, arr) => (
            <Styled.ListItem key={i}>
              <Styled.ListItemLeft>
                <Styled.Text color="#9D9F9F" size="10px">
                  {item.name}
                </Styled.Text>
                <Styled.Text color="#515353" size="13px">
                  +994 {item.phone}
                </Styled.Text>
              </Styled.ListItemLeft>
              <TouchableOpacity
                onPress={() => {
                  dispatch({
                    type: 'SHARED_USERS',
                    users: arr.filter(f => f.phone !== item.phone),
                  });
                }}>
                <Styled.Icon
                  type="Feather"
                  name="trash"
                  color="#aaa"
                  size="16px"
                />
              </TouchableOpacity>
            </Styled.ListItem>
          ))}
      </Styled.List>
    </React.Fragment>
  );
};

const mapStateToProps = state => ({
  name: state.shareContacs.users,
});
const mapDispatchToProps = dispatch => ({
  dispatch,
});
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(List);
