import React from 'react';
import {Navigation} from 'react-native-navigation';
import {Provider} from './store/Provider';
import {InitializingScreen, INITIALIZING_SCREEN} from './screens/Initializing';
import {setRoot} from './services/navigation';

// SCREENS
import StartScreen from './screens/StartScreen';
import PinScreen from './screens/PinScreen';
import Dashboard from './screens/Dashboard';
import Share from './screens/Share';
import TopbarLeft from './screens/TopbarLeft';
import TopbarRight from './screens/TopbarRight';
import ContactScreen from './screens/Contact';

function WrappedComponent(Component) {
  return function inject(props) {
    const EnhancedComponent = () => (
      <Provider>
        <Component {...props} />
      </Provider>
    );

    return <EnhancedComponent />;
  };
}

export const registerScreens = () => {
  Navigation.registerComponent(INITIALIZING_SCREEN.name, () =>
    WrappedComponent(InitializingScreen),
  );
  Navigation.registerComponent('StartScreen', () =>
    WrappedComponent(StartScreen),
  );
  Navigation.registerComponent('PinScreen', () => WrappedComponent(PinScreen));
  Navigation.registerComponent('Dashboard', () => WrappedComponent(Dashboard));
  Navigation.registerComponent('Share', () => WrappedComponent(Share));
  Navigation.registerComponent('TopbarLeft', () =>
    WrappedComponent(TopbarLeft),
  );
  Navigation.registerComponent('TopbarRight', () =>
    WrappedComponent(TopbarRight),
  );
  Navigation.registerComponent('ContactScreen', () =>
    WrappedComponent(ContactScreen),
  );
};

export const goToAuthScreen = () =>
  setRoot('stack', 'SignIn', [
    {
      component: {
        name: 'StartScreen',
        options: {
          topBar: {
            visible: false,
          },
        },
      },
    },
  ]);

export const goToHomeScreen = () =>
  setRoot('stack', 'App', [
    {
      component: {
        name: 'Dashboard',
        options: {
          topBar: {
            background: {
              color: '#D8FADD',
              translucent: true,
            },
            title: {
              component: {
                name: 'TopbarLeft',
                alignment: 'fill',
              },
            },
          },
        },
      },
    },
  ]);
